	var status1, status2;
	
	$(function(){
		setStartTime3();
		setElement3();
		statusChart("status1");
		statusChart("status2");
		popup("popupChart");
		
		pieChart("pie1");
		pieChart("pie2");
		
		drawGaugeChart("opTime1", "cuttingTime1", "运转率", "实际加工率","%");
		drawGaugeChart("spdLoad1", "feedOverride1", "Spindle Load", "Feed Override","");
		
		drawGaugeChart("opTime2", "cuttingTime2", "运转率", "实际加工率","%");
		drawGaugeChart("spdLoad2", "feedOverride2", "Spindle Load", "Feed Override","");
		detailBar = $("#popupChart").highcharts();
		setInterval(time, 1000);
		
		detailBar = $("#popupChart").highcharts();
		detailOptions = detailBar.options;
		
		getAllDvcId();
		
		var pie1 = $("#pie1").highcharts();
		var pie2 = $("#pie2").highcharts();
		
		for(var i=0; i<=4; i++){
			pie1.series[0].data[i].update(0);
			pie2.series[0].data[i].update(0);
		};
		
//		$("html").click(function(){
//			$("#chartDataBox").fadeOut();
//			$("#popup").fadeOut();
//		});
		setInterval(time3,1000);
		
		
	});
	
	function time3(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date3").html(month + " / " + date_ + " (" + day + ")");
		$("#time3").html(hour + " : " + minute + " : " + second);
	};
	
	function parsingModal(obj, idx){
		var json = $.parseJSON(obj);
	
		var auxCode = json.AUX_CODE;
		var gModal = json.G_MODAL;
		
		var auxTR = "<tr>";
		var auxTD;
		var gModalTR = "<tr>";
		var gModalTD;
		
		$.each(auxCode, function(key, data){
			$.each(auxCode[key], function (key, data){
				auxTD += "<td>" + key + " : " + data + "</td>"; 
			});
		}); 
		
		auxTR += auxTD + "</tr>";
		
		$.each(gModal, function(key, data){
			$.each(gModal[key], function (key, data){
				gModalTD += "<td>" + key + " : " + data + "</td>";
			});
		}); 
		
		gModalTR += gModalTD + "</tr>";
		
		$("#modalTbl" + idx).append(auxTR);
		$("#modalTbl" + idx).append(gModalTR);
	};

	function popup(id){
		$('#' + id).highcharts({
			chart : {
				
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200
			},
			credits : false,
			//exporting: false,
			title : false,
			xAxis : {
				categories : [""],
				labels : {
					style : {
						fontSize : '35px',
						fontWeight:"bold",
						color : "white"
					}
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			yAxis : {
				min : 0,
				max : 59,
				tickInterval:1,
				reversedStacks: false,
				title : {
					text : false
				},
				labels: {
	                formatter: function () {
	                	var value = labelsArray_hour[this.value]
	                    return value;
	                },
	                style :{
	                	color : "black",
	                	fontSize : "20px"
	                },
	            },
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				series : {
					  dataGrouping : {
		                    forced : true,
		                    units : [['minute', [5]]]
		                },
					stacking : 'normal',
					pointWidth:120, 
					borderWidth: 0,
					animation: false,
					cursor : 'pointer',
					point : {
						events: {
							click: function (e) {
		                   	}
		              	}
					}
				},
			},
			series : [{
				data :[0.16],
				color : "Red"
			},
			{
				data :[0.16],
				color : "green"
			},
			{
				data :[0.16],
				color : "yellow"
			},
			{
				data :[0.16],
				color : "Red"
			},
			{
				data :[0.16],
				color : "green"
			},
			{
				data :[0.16],
				color : "Red"
			}]
		});
	};
	
	var labelsArray_hour = [0,1,2,3,4,5,6,7,8,9,
	                        10,11,12,13,14,15,16,17,18,19,
	                        20,21,22,23,24,25,26,27,28,29,
	                        30,31,32,33,34,35,36,37,38,39,
	                        40,41,42,43,44,45,46,47,48,49,
	                        50,51,52,53,54,55,56,57,58,59
	                        ];
	
	var dvcArray = new Array();
	var dvcIndex = 0;
	function getAllDvcId(){
		var url = ctxPath + "/chart/getAdapterId.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			dataType : "json",
			data : param,
			type : "post",
			success : function(data){
				var json = data.dvcId;
				$(json).each(function(i, obj){
					var dvc = new Array();
					dvc.push(obj.dvcId, obj.name);
					dvcArray.push(dvc);
				});
				
				getDvcStatus(dvcArray[dvcIndex], 1);
				getDvcStatus(dvcArray[dvcIndex+1], 2);
				
				setInterval(resetArray,1000 * 20);
				
				currentStatusLoop1= setInterval(function(){getCurrentDvcStatus(dvcArray[dvcIndex][0], 1)}, 3000)
				currentStatusLoop2= setInterval(function(){getCurrentDvcStatus(dvcArray[dvcIndex+1][0], 2)}, 3000)
			}
		});
	};
	
	function resetArray(){
		dvcMap1.put("flag", true);
		dvcMap1.put("initFlag", true);
		dvcMap1.put("currentFlag", true);
		clearInterval(currentStatusLoop1);
		clearInterval(statusLoop1);
		dvcMap2.put("flag", true);
		dvcMap2.put("initFlag", true);
		dvcMap2.put("currentFlag", true);
		clearInterval(currentStatusLoop2);
		clearInterval(statusLoop1);
		
		dvcIndex+=2;
		if(dvcIndex>=dvcArray.length)dvcIndex=0;
		
		getDvcStatus(dvcArray[dvcIndex], 1);
		getDvcStatus(dvcArray[dvcIndex+1], 2);
	};
	var statusLoop1, statusLoop2;
	var currentStatusLoop1, currentStatusLoop2;
	
	function isItemInArray(array, item) {
		for (var i = 0; i < array.length; i++) {
			for (var j = 0; j < array[i].length; j++) {
				if (array[i][j] == item) {
					return true;   // Found it
		        };
	        };
	    };
		return false;   // Not found
	};
	
	var block = 1/6;
	function getDvcStatus(dvc, idx){
		var dvcId = dvc[0];
		var name = dvc[1];
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		
		var url = ctxPath + "/chart/getTimeChart.do";
		var param = "dvcId=" + dvcId + 
					"&targetDateTime=" + today;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				$("#loader").css("opacity", 0);
				$("#mainTable").css("opacity", 1);
					
				if(data==null || data==""){
					eval("dvcMap" + idx).put("noSeries", true);
					getCurrentDvcStatus(dvcId, idx);
					return;
				};
				
				var json = $.parseJSON(data);
				
				var status = $("#status" + idx).highcharts();
				var options = status.options;
				
				options.series = [];
				options.title = null;
				
				$(json).each(function (i, data){
					var bar = data.data[0].y;
					var startTime = data.data[0].startTime;
					var endTime = data.data[0].endTime;
					var color = eval(data.color);
					
					if(eval("dvcMap" + idx).get("flag")){
						options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : color
					        	}],
					    });	
					}else{
						options.series[0].data.push({
							y : Number(200),
							segmentColor : color
						});
					};
					eval("dvcMap" + idx).put("flag", false);
				});  
				
				status = new Highcharts.Chart(options);
				
				getCurrentDvcStatus(dvcId, idx);
			}
		});
	};

	var labelsArray = [20,21,22,23,24,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	
	function drawPieData(chartStatus, inCycleTime, waitTime, alarmTime, offTime, chart){
		chart.series[0].data[0].update(inCycleTime);		
		chart.series[0].data[1].update(waitTime);
		chart.series[0].data[2].update(alarmTime);
		chart.series[0].data[3].update(offTime);
		
		var day = 24*60*60;
		var blank = day-(inCycleTime + waitTime + alarmTime + offTime);
		chart.series[0].data[4].update(blank);
	};
	
	function calcTime(startTime, endTime){
		var startH = Number(startTime.substr(0,2));
		var startM = Number(startTime.substr(3,2));
		var startS = Number(startTime.substr(6,2));

		var endH = Number(endTime.substr(0,2));
		var endM = Number(endTime.substr(3,2)); 
		var endS = Number(endTime.substr(6,2));
		
		return ((endH*60*60) + (endM*60) + endS) - ((startH*60*60) + (startM*60) + startS);  
	};
	
	function removeSpace(str){
		return str = str.replace(/ /gi, "");
	};
	
	var dvcMap1 = new JqMap();
	var dvcMap2 = new JqMap();
	
	dvcMap1.put("flag", true);
	dvcMap1.put("initFlag", true);
	dvcMap1.put("currentFlag", true);
	
	dvcMap2.put("flag", true);
	dvcMap2.put("initFlag", true);
	dvcMap2.put("currentFlag", true);
	
	function getCurrentDvcStatus(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var workDate = year + "-" + month + "-" + day;
		
		var url = ctxPath + "/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + "&workDate=" + workDate;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				
				$("#alarmCode" + idx +"_0").html("");
				$("#alarmMsg" + idx +"_0").html("");
				$("#alarmCode" + idx +"_1").html("");
				$("#alarmMsg" + idx +"_1").html("");
				//parsingAlarm(idx, alarm);

				var lamp = "<img src=" +ctxPath + "/images/DashBoard/" + chartStatus +".png style='height:" + getElSize(25) + ";vertical-align: text-top;'>";
				$("#progName" + idx).html(progName);
				$("#progHeader" + idx).html(progHeader);
				$("#3_dvcName" + idx).html(name + lamp);
				
				
				if(eval("dvcMap" + idx).get("currentFlag")){
					var status = $("#status" + idx).highcharts();
					var options = status.options;
					
					if(eval("dvcMap" + idx).get("noSeries")){
						options.series = [];
						options.title = null;
						
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });	
					}else{
						options.title = null;
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		
					};
					
		      	  	var now = options.series[0].data.length;
					var blank = 144 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					
					eval("dvcMap" + idx).put("currentFlag", false)
		      	};
		      	
				var pieChart = $("#pie" + idx).highcharts();
				drawPieData(chartStatus, inCycleTime, waitTime, alarmTime, noConTime, pieChart);
				
				var opTimeChart = $("#opTime" + idx).highcharts().series[0].points[0];
				opTimeChart.update(Number(opRatio));
			
				var cuttingTime = $("#cuttingTime" + idx).highcharts().series[0].points[0];
				cuttingTime.update(Number(cuttingRatio));
				
				var spd = $("#spdLoad" + idx).highcharts().series[0].points[0];
				spd.update(Number(spdLoad));
				
				var feed = $("#feedOverride" + idx).highcharts().series[0].points[0];
				feed.update(Number(feedOverride));
				
				var statusLoop;
				if(idx==1){
					statusLoop = currentStatusLoop1;
				}else{
					statusLoop = currentStatusLoop1;
				};
			}
		});
		
	};
	
	function parsingAlarm(idx,alarm){
		var json = $.parseJSON(alarm);
		
		$(json).each(function (i,data){
			$("#alarmMsg" + idx + "_" + i).html(data.alarmCode + " - " + data.alarmMsg);
		});
	};
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	function drawGaugeChart(el1, el2, title1, title2, unit){
		var gaugeOptions = {
				chart: {
					type: 'solidgauge',
			     	backgroundColor : 'rgba(255, 255, 255, 0)',
			  	},
			  	title : false,
				credits : false,
			   	exporting : false,
			 	pane: {
			 		center: ['50%', '60%'],
			      	size: '100%',
	         		startAngle: -90,
	         		endAngle: 90,
	         		background: {
	             	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	             	innerRadius: '60%',
	             	outerRadius: '100%',
	             	shape: 'arc'
	         		}
			 	},
        		tooltip: {
        			enabled: false
			  	},

			  	// the value axis
			 	yAxis: {
			 		stops: [
			 		        [1, '#13E000']
			            	],
			            	lineWidth: 0,
			            	minorTickInterval: null,
			            	tickPixelInterval: 400,
			            	tickWidth: 0,
			            	title: {
			                y: -getElSize(70)
			            	},
			            labels: {
			                y: 16
			            	}
			 	},
			 	plotOptions: {
			            solidgauge: {
			                dataLabels: {
			                    y: 5,
			                    borderWidth: 0,
			                    useHTML: true
			                }
			            }
			        }
			    };

				var max;
				if(unit=="%"){
					max = 100
				}else{
					max = 200;
				};
			    // The speed gauge
			    $('#' + el1).highcharts(Highcharts.merge(gaugeOptions, {
			    	  yAxis: {
				            min: 0,
				            max: max,
				            title: {
				                text: title1,
				               y: -getElSize(55),
				               style:{
				            	   color : "black",
				            	   fontSize : getElSize(15),
									fontWeight:"bold"
				            	   
				               }
				    		},
				        },

			        credits: {
			            enabled: false
			        },

			        series: [{
			            name: '가동률',
			            data: [0],
			            dataLabels: {
			                format: '<div style="text-align:center"><span style="font-size:' +getElSize(25) + 'px;color:white">{y}</span>' +
			                       '<span style="font-size:' + getElSize(15) + 'px;color:white">' + unit + '</span></div>'
			            },
			            tooltip: {
			                valueSuffix: ' %'
			            }
			        }]

			    }));

			    // The RPM gauge
			    $('#' + el2).highcharts(Highcharts.merge(gaugeOptions, {
			    	 yAxis: {
				            min: 0,
				            max: max,
				            title: {
				                text: title2,
				               y: -getElSize(55),
				               style:{
				            	   color : "black",
				            	   fontSize : getElSize(15),
									fontWeight:"bold"
				               }
									
				            },
				        },

			        series: [{
			            name: '절분률',
			            data: [0],
			            dataLabels: {
			                format: '<div style="text-align:center"><span style="font-size:' + getElSize(25) + 'px;color:white">{y}</span>' +
			                       '<span style="font-size:' + getElSize(15) + 'px;color:white">' + unit + '</span></div>'
			            },
			            tooltip: {
			                valueSuffix: '%'
			            }
			        }]

			    }));
	};
	
	function pieChart(id){
		Highcharts.setOptions({
			//green yellow red black
			   colors: ['#148F01', '#C7C402', '#ff0000', '#8C9089', "#ffffff"]
		    });
		
	    
	    // Radialize the colors
	    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
	        return {
	            radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
	            stops: [
	                [0, Highcharts.Color(color).brighten(0.5).get('rgb')], // darken
	                [1, color]
	                
	            ]
	        };
	    });
	    
		$('#' + id)
		.highcharts(
				{
					chart : {
						plotBackgroundColor : null,
						plotBorderWidth : null,
						plotShadow : false,
						backgroundColor : 'rgba(255, 255, 255, 0)',
						type: 'pie',
			            options3d: {
			                enabled: true,
			                alpha: 45
			            }
					},
					credits : false,
					title : {
						text : false
					},
					tooltip : {
						pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>',
						enabled : false
					},
					plotOptions : {
						pie : {
							innerSize: getElSize(130),
				      		depth: getElSize(45),
							size:'100%',
							allowPointSelect : true,
							cursor : 'pointer',
							dataLabels : {	 
								enabled : true,
							 	formatter : function(){
							 		var value;
							 		if(this.y!=0.0) value = Number(this.y/60/60).toFixed(1);
							 		return value;
							 	},
							 	connectorColor: '#000000',
								distance : 1,
							 	style : {
							 		 color: 'black',
							 		 textShadow: '0px 1px 2px black',
							 		 fontSize : getElSize(12),
								}
							}
						}
					},
					exporting: false,
					series : [ {
						type : 'pie',
						name : 'Chart Status',
						data : [ [ 'In-Cycle',1], 
						         [ 'Wait', 1 ],
						         [ 'Alarm', 1 ],
						         [ 'Power-Off', 1],
						         [ 'blank', 1],
								]
					} ]
				});
		
//		$("#pieChart1").css({
//			"left" : $("#container").offset().left + getElSize(300),
//			"top": $("#container").offset().top + contentHeight/(targetHeight/1480),
//			"height": contentHeight/(targetHeight/700),
//			"width": getElSize(750)
//		});
	};
	
	var detailOptions;
	var detailBarArray = new Array();
	function addDetailBar(bar, color){
		var _bar = new Array();
		_bar.push(bar);
		_bar.push(color);
		
		detailBarArray.push(_bar);
	};
	
	function reDrawDetailBar(){
		detailOptions.series = [];
		detailOptions.title = null;
		for (var i = 0; i < detailBarArray.length; i++){
			detailOptions.series.push({
		        data: [{
		        		y : detailBarArray[i][0],
		        	}],
		        color : detailBarArray[i][1]
		    });	
		};
		
		detailBar = new Highcharts.Chart(detailOptions);
	};
	
	var barSize = 1/6;
	var detailBarMap = new JqMap();
	
	function getDetailStatus(dvcId, dvcName, hour){
		if(detailBarArray.length!=0){
			detailBarArray = new Array();
			for(var i = 0; i < detailBarArray.length; i++){
				detailBar.series[0].remove(true);
			};			
		};
		
		if(hour==24) hour=0;
		
		var url = ctxPath + "/chart/getDetailStatus.do";
		var data = "dvcId=" + dvcId + 
					"&startDateTime=" + hour;
		
		$.ajax({
			url : url,
			data : data,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.chartStatus;
				
				$(json).each(function(i, data){
					var startTime = removeHypen(data.startDateTime, "690").substr(9,8);
					var endTime = removeHypen(data.endDateTime, "691").substr(9,8);
					var color = slctChartColor(removeSpace(data.chartStatus));
					
					var startH = Number(startTime.substr(0,2));
					var startM = String(startTime.substr(3,2));
					var startS = Number(startTime.substr(6,2));

					var endH = Number(endTime.substr(0,2));
					var endM = String(endTime.substr(3,2)); 
					var endS = Number(endTime.substr(6,2));

					var start = (startH*60*60) + (startM*60) + (startS);
					var end = (endH*60*60) + (endM*60) + (endS);
					
					if(i==0)start=hour*60*60;
					var bar = (end - start)/10*barSize;
					
					if(bar<0.16)bar=0.16;
					detailBarMap.put("chart", detailBar);
					
					console.log(startTime, endTime, removeSpace(data.chartStatus), bar);
					addDetailBar(bar, color)
				});
				
				reDrawDetailBar();
				
				$("#popup").fadeToggle();
				$("#hour").html(dvcName + " (" + hour + ":00 ~ " + hour + ":59)");	
			}
		});
	};
	
	function setStartTime3(){
		var url = ctxPath + "/chart/getStartTime.do"
		
		$.ajax({
			url :url,
			dataType :"text",
			type : "post",
			success : function(data){
				var startTime = Number(data);
				for(var i = 0; i<=24; i++){
					if(i+startTime>24){
						startTime -= 24;
					};
					
					timeLabel3.push(i+startTime);
					for(var j = 0; j < 5; j++){
						timeLabel3.push(0);	
					};
				};		
			}
		});
	};
	
	var timeLabel3 = [];
	
	var colors;
	function statusChart(id){
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
	//	$('#' + id).highcharts({
		options = {
			chart : {
				
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: getElSize(150),
				marginTop: -100
			},
			credits : false,
			exporting: false,
			title : false,
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			xAxis:{
		           categories:timeLabel3,
		            labels:{
		               
		                 formatter: function () {
			                        	var val = this.value
			                        	if(val==0){
			                        		val = "";
			                        	};
			                        	return val;   
			                        },
			                        style :{
			    	                	color : "white",
			    	                	fontSize : getElSize(8) + "px",
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}
		
	   	$("#" +id).highcharts(options);
	};
	
	function setElement3(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#title_main3").css({
			"width": getElSize(500)
		});
		
		$("#title_main3").css({
			"left" : width/2*0.5 - $("#title_main2").width()/2,
			"margin-top" : (getElSize(25))
		});
		
		$("#title_left3").css({
			"width" : getElSize(150)
		});
		
		$("#title_left3").css({
			"left" : getElSize(25),
			"margin-top" : (getElSize(25))
		});

		$("#title_right3").css({
			"color" : "white",
			"font-weight" : "bolder",
			"font-size" : getElSize(20),
			"margin-top" : getElSize(40),
			"position" : "absolute"
		});

		$("#title_right3").css({
			"left" : contentWidth/2 - $("#title_right3").width() - getElSize(25)        
		});
	
		$("#time3").css({
			"right" : getElSize(25),
			"top" : (getElSize(85)),
			"font-size" : getElSize(15)
		});
		
		$("#date3").css({
			"right" : getElSize(150),
			"top" : (getElSize(85)),
			"font-size" : getElSize(15)
		});
		
		
		$("#hr3").css({
			"margin-top" : getElSize(60)
		});
		
		$("#mainTable3").css({
			"width" : $("#part3").width()*0.95
		});
		
		$("#mainTable3").css({
			"margin-left" : contentWidth/2*0.5 - $("#mainTable3").width()/2 + sideMargin
		});
		
		$(".status3").css({
			"width" : contentWidth*0.45*0.5,
			"margin-top" : getElSize(20)
		});
		
		$(".td3").css({
			"padding" : "0 " + getElSize(25) + " 0" + getElSize(25)
		});
		
		$(".tableText3").css({
			"font-size" : getElSize(25)
		});
		
		$(".mode").css({
			"font-size" : getElSize(20),
			"margin-right" : getElSize(10)
		});
		
		$(".td_title3").css({
			"font-size" : getElSize(20),
			"padding" : getElSize(10)
		});
		
		$(".pie_tr").css({
			"height" : getElSize(600)
		});
		
		$(".gauge_tr").css({
			"height" :getElSize(200),
			"margin-top" : getElSize(50)
		});
		
//		$(".alarmDiv").css({
//			"margin-left":getElSize(25),
//			"margin-right":getElSize(25),
//			"margin-bottom":getElSize(15),
//			"line-height": getElSize(25)
//		});
		
		$(".alarmText").css({
			"font-size" : getElSize(15)
		});
		
		$(".pie").css({
			"height" : getElSize(500)
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};