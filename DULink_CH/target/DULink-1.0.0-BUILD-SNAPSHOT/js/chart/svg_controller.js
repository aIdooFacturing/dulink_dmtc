var shopId = 2;
var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";

var pieChart1;
var pieChart2;
var borderColor = "";
var totalOperationRatio = 0;

$(function() {
	pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();

	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	draw = SVG("svg");
	
	//getMarker();
	
	var timeStamp = new Date().getMilliseconds();
	
	background_img = draw.image(imgPath+"Road.png?dummy=" + timeStamp).size(contentWidth * 0.9, contentHeight);
	background_img.x(getElSize(100));
	background_img.y(-getElSize(100));
	
	var logoText =  draw.text(function(add) {
		  add.tspan("努力迈向金属切削行业全世界前三").fill("#000000");
		  
	});
		
	logoText.font({
			'family':'Helvetica'
			, size:     getElSize(130)
			, anchor:   'right'
			, leading:  '1.3em'
		   ,'font-weight':'bolder',
	});
	
	logoText.x(getElSize(1450));
	logoText.y(getElSize(1800));
	
	getMachineInfo();
});

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;

			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				if(data.lastChartStatus.toUpperCase()=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus.toUpperCase()=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus.toUpperCase()=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus.toUpperCase()=="NO-CONNECTION"){
					powerOffMachine++;
				}
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "green";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			bodyNeonEffect(borderColor);
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId){
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:     fontSize * 1.8
		, anchor:   'middle'
		, leading:  '0em'
	});
	
	if(arrayIdx==43){
		machineName[arrayIdx].click(function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			var today = year + "-" + month + "-" + day;
			var url = ctxPath + "/device/getNightlyMachineStatus.do";
			var param = "stDate=" + $("#sDate").val() + 
						"&edDate=" + $("#sDate").val();
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			if(!table_8){
				url = ctxPath + "/device/getNightlyMachineStatus_8.do";
				param = "stDate=" + today + 
						"&edDate=" + today;
				$("#tableSubTitle").html("기준 시간 : 18시 ~ 08시");
			};
			
			$.ajax({
				url : url,
				data: param,
				dataType : "text",
				type : "post",
				success : function(data){
					var json = $.parseJSON(data);
					
					var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
								"<td class='td'>No</td>" + 
								"<td class='td'>설비명</td>" + 
								"<td class='td'>날짜</td>" + 
								"<td class='td'>정지시간</td>" + 
								"<td class='td'>상태</td>" + 
							"</tr>";
					csvData = "NO., 설비명, 날짜, 정지시간, 상태LINE";
					var status = "";
					var fontSize = getElSize(25);
					var backgroundColor = "";
					var fontColor = "";

					$(json).each(function (idx, data){
						if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
							status = "완료";
							fontColor = "black";
							backgroundColor = "yellow";
						}else if(data.status=='ALARM'){
							status = "중단";
							fontColor = "white";
							backgroundColor = "red";
						}else if(data.status=='IN-CYCLE'){
							status = "가동";
							fontColor = "white";
							backgroundColor = "green";
						};
						
						if(data.isOld=="OLD"){
							status = "미가동";
							fontColor = "white";
							backgroundColor = "black";
						};
						
						tr += "<tr class='contentTr'>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
								"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
							"</tr>";
						csvData += (idx+1) + "," +
									data.name + "," + 
									data.stopDate + "," + 
									data.stopTime + "," + 
									status + "LINE";
					});
					
					if(json.length==0){
						tr += "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
								"<td colspan='5'>없음</tb>" + 
							"<tr>";
					};
					
					$("#table").html(tr);
					$(".tr").css("font-size", getElSize(30));
					$(".td").css("padding", getElSize(10));
					
					$("#tableDiv").show();
					table_8 = !table_8;
					table_flag = true;
				}
			});
		});
	};
	
	machineName[arrayIdx].dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			
			location.href=ctxPath + "/chart/singleChartStatus.do";
		};
	})
	
	var wMargin = 0;
	var hMargin = 0;
	
	if(name=="SET UP"){
		hMargin = contentHeight/(targetHeight/30);
	}else if(name.indexOf("WASHING")!=-1){
		wMargin = contentHeight/(targetHeight/10);
	}else if(name.indexOf("NHM")!=-1){
		wMargin = getElSize(-10);
	}
	
	if(name.indexOf("AH30")!=-1){
		hMargin = -getElSize(50);
	}else if(name.indexOf("HF5M")!=-1){
		wMargin = getElSize(-50);
	}

	machineName[arrayIdx].x(x + (w/2) - wMargin);
	machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/70)+hMargin));
	
	machineName[arrayIdx].leading(1);
};

function nl2br(value) { 
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;
function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			newMachineStatus = new Array();
			
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(getElSize(data.x));
				array.push(getElSize(data.y));
				array.push(getElSize(data.w));
				array.push(getElSize(data.h));
				array.push(data.pic);
				array.push(data.lastChartStatus.toUpperCase());
				array.push(data.dvcId);
				array.push(getElSize(data.fontSize));
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus.toUpperCase()=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus.toUpperCase()=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus.toUpperCase()=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus.toUpperCase()=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
			
			if(!compare(machineStatus,newMachineStatus)){
				clearInterval(border_interval);
				reDrawMachine();
				redrawPieChart();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fontSize
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8],
							machineProp[i][9]);
				};
				first = false;
			};
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
			var totalMachine = 0;
			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
//			pieChart1.series[0].data[0].update(Number(totalOperationRatio));
//			pieChart1.series[0].data[1].update(100-Number(totalOperationRatio));
		}
	});
	
	setTimeout(getMachineInfo, 3000);
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize){
	var svgFile;
	var timeStamp = new Date().getMilliseconds();
	if(status==null){
		svgFile = ".svg";
	}else{
		svgFile = "_" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status.toUpperCase()=="WAIT" || status.toUpperCase()=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	//idx, x, y, w, h, name
	printMachineName(i, x, y, w, h, name, text_color, fontSize, dvcId);
	
	//idx, machineId, w, h
	//setDraggable(i, id, w, h);
	
	if(id==46){
		machineList[i].click(function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			var today = year + "-" + month + "-" + day;
			var url = ctxPath + "/device/getNightlyMachineStatus.do";
			var param = "stDate=" + $("#sDate").val() + 
						"&edDate=" + $("#sDate").val();
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			if(!table_8){
				url = ctxPath + "/device/getNightlyMachineStatus_8.do";
				param = "stDate=" + today + 
						"&edDate=" + today;
				$("#tableSubTitle").html("기준 시간 : 18시 ~ 08시");
			};
			
			$.ajax({
				url : url,
				data: param,
				dataType : "text",
				type : "post",
				success : function(data){
					var json = $.parseJSON(data);
					
					var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
								"<td class='td'>No</td>" + 
								"<td class='td'>설비명</td>" + 
								"<td class='td'>날짜</td>" + 
								"<td class='td'>정지시간</td>" + 
								"<td class='td'>상태</td>" + 
							"</tr>";
					csvData = "NO., 설비명, 날짜, 정지시간, 상태LINE";
					var status = "";
					var fontSize = getElSize(25);
					var backgroundColor = "";
					var fontColor = "";

					$(json).each(function (idx, data){
						if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
							status = "완료";
							fontColor = "black";
							backgroundColor = "yellow";
						}else if(data.status=='ALARM'){
							status = "중단";
							fontColor = "white";
							backgroundColor = "red";
						}else if(data.status=='IN-CYCLE'){
							status = "가동";
							fontColor = "white";
							backgroundColor = "green";
						};
						
						if(data.isOld=="OLD"){
							status = "미가동";
							fontColor = "white";
							backgroundColor = "black";
						};
						
						tr += "<tr class='contentTr'>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
								"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
							"</tr>";
						csvData += (idx+1) + "," +
									data.name + "," + 
									data.stopDate + "," + 
									data.stopTime + "," + 
									status + "LINE";
					});
					
					if(json.length==0){
						tr += "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
								"<td colspan='5'>없음</tb>" + 
							"<tr>";
					};
					
					$("#table").html(tr);
					$(".tr").css("font-size", getElSize(30));
					$(".td").css("padding", getElSize(10));
					
					$("#tableDiv").show();
					table_8 = !table_8;
					table_flag = true;
				}
			});
		});
	};
	
	machineList[i].dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			
			location.href=ctxPath + "/chart/singleChartStatus.do";
		};
	})
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};


function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
		machineName[arrayIdx].x(x + (w/2) - getElSize(30));
		machineName[arrayIdx].y(y + (h/2) - getElSize(10));
		
		//DB Access
		setMachinePos(id, x, y);
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};