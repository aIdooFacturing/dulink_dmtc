<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highchart.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<style>

@font-face {font-family:MalgunGothic; src:url(../fonts/malgun.ttf);}

*{
	font-family:'MalgunGothic';
}
#chart{
	overflow: hidden;
}

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}

#title_main{
	left: 1300px;
	width: 1000px;
}

#title_left{
	left : 50px;
	width: 300px;
}


#pieChart1{
	position: absolute;
	left : 800px;
	z-index: 99;
	top: 1480px;
	height: 700px;
	width: 750px;
}

#pieChart2{
	position: absolute;
	left : 300px;
	z-index: 99;
	height: 700px;
	width: 750px;
}

#tableDiv{
	left: 20px;
	width: 600px; 
	position: absolute; 
	z-index: 999;
	top: 450px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}

#time{
	position: absolute;
	top: 170px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#logo{
	position: absolute;
	top: 170px;
	right: 60px;	
}
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 
.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style> 
<script type="text/javascript">
	var loopFlag = null;
	var session = window.sessionStorage.getItem("auto_flag");
	if(session==null) window.sessionStorage.setItem("auto_flag", true);
	
	function stopLoop(){
		var flag = window.sessionStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
		}else{
			flag = "true"
		}
		
		window.sessionStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("Auto screen chang : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			closePanel();
			panel = false;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else if(type=="menu2"){
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		}else if(type=="menu3"){
			url = "${ctxPath}/chart/DIMM.do";
			location.href = url;
		}else if(type=="menu4"){
			url = "${ctxPath}/chart/main3.do";
			location.href = url;
		};
	};
	
	var openCal = false;
	function showCalendar(){
		var opacity;
		if(!openCal){
			opacity = 1;
		}else{
			opacity = 0;
		};
		openCal = !openCal;
		
		$("#sDate, #date_controller").animate({
			"opacity" : opacity
		});
	};
	
	var table_flag = true;
	var table_8 = false;
	
	function dateUp(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		changeDateVal();
	};
	
	function changeDateVal(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		if(today==$("#sDate").val()){
			isToday = true;
		}else{
			isToday = false;
		};
		
		getNightlyMachineStatus();
		table_8 = false;
	}
	
	function dateDown(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		changeDateVal();
	};
	
	$(function(){
		$("#up").click(dateUp);
		$("#down").click(dateDown);
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = date.getMinutes();
		
		$("#excel").click(csvSend);
		$("#sDate").val(year + "-" + month + "-" + day);
		$("#sDate").change(changeDateVal);
		
		$("#tableTitle").click(showCalendar);
		$(".menu").click(goReport);
		//getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		document.oncontextmenu = function() {stopLoop()}; 
		setDivPos();
		setInterval(time, 1000);
		
		$("#table_toggle_btn").click(function(){
			$("#tableSubTitle").html("基准时间 : 18时 ~ 07时");
			table_8 = false;
			getNightlyMachineStatus();
			if(table_flag){
				$("#tableDiv").hide();				
			}else{
				$("#tableDiv").show();
			}
			table_flag=!table_flag;
		});
		
		if(hour>=7 && (hour<=8 && minute<=30)){
			$("#tableDiv").show();
			table_flag = true;
		}else{
			$("#tableDiv").hide();
			table_flag = false;
		};
		
		/* $("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		}); */
		
		$("#title_right").click(function(){
			location.href = "${ctxPath}/chart/main3.do";
		}); 
		
		/* $("#title_left").click(function(){
			location.href = "${ctxPath}/chart/main3.do";
		}); */
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		//getNightlyMachineStatus();	
		
		if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		$("#pieChart2").css({
			"left" : $("#container").offset().left + getElSize(300),
			//"top": $("#container").offset().top + contentHeight/(targetHeight/1480),
			"height": contentHeight/(targetHeight/700),
			"width": getElSize(750)
		});
		
		$(".menu").click(goReport);
		$("#menu0").addClass("selected_menu");
		$("#menu0").removeClass("unSelected_menu");
	});
	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			//location.href = ctxPath + "/chart/multiVision.do";
			
			window.sessionStorage.setItem("dvcId", "107");
			window.sessionStorage.setItem("name", "HF4M");
			
			location.href=ctxPath + "/chart/singleChartStatus.do";
			
		},1000*20);
	};
	
	var isToday = true;
	function getNightlyMachineStatus(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var workDate = "";
		if(isToday){
			workDate = today;
		}else{
			workDate = $("#sDate").val();
		};
		
		
		var url = "${ctxPath}/device/getNightlyMachineStatus.do";
		var param = "stDate=" + workDate + 
					"&edDate=" + workDate + 
					"&shopId=" + shopId;
		
		
		if(table_8){
			param = "stDate=" + today + 
					"&edDate=" + today + 
					"&shopId=" + shopId;
		};
		
		console.log(workDate)
		$.ajax({
			url : url,
			data: param,
			dataType : "text",
			type : "post",
			success : function(data){
				var json = $.parseJSON(data);
				var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
							"<td class='td'>No</td>" + 
							"<td class='td'>设备名</td>" + 
							"<td class='td'>日期</td>" + 
							"<td class='td'>停机时间</td>" + 
							"<td class='td'>状态</td>" + 
						"</tr>";
				csvData = "NO., 设备名, 日期, 停机时间, 状态LINE";
				var status = "";
				var fontSize = getElSize(25);
				var backgroundColor = "";
				var fontColor = "";
				
				$(json).each(function (idx, data){
					if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
						status = "结束";
						fontColor = "black";
						backgroundColor = "yellow";
					}else if(data.status=='ALARM'){
						status = "中断";
						fontColor = "white";
						backgroundColor = "red";
					}else if(data.status=='IN-CYCLE'){
						status = "运转";
						fontColor = "white";
						backgroundColor = "green";
					};
					
					if(data.isOld=="OLD"){
						status = "非运转";
						fontColor = "white";
						backgroundColor = "black";
					};
					
					tr += "<tr class='contentTr'>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
							"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
						"</tr>";
					csvData += (idx+1) + "," +
								data.name + "," + 
								data.stopDate + "," + 
								data.stopTime + "," + 
								status + "LINE";
				});
				
				if(json.length==0){
					tr += "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
							"<td colspan='5'>无</tb>" + 
						"<tr>";
				};
				
				$("#table").html(tr);
				$(".tr").css("font-size", getElSize(30));
				$(".td").css("padding", getElSize(10));
				
				setTimeout(function(){
					getNightlyMachineStatus();					
				}, 1000*60*1);
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){ 
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
			"margin-top" : height/2 - ($("#container").height()/2)
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		
		
		$("#title_main").css({
			"width" : getElSize(1000),
			"top" : $("#container").offset().top + (getElSize(50))
		});

		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_left").css({
			"left" : $("#container").offset().left + getElSize(50)
		});
		
		$("#title_right").css({
			"right" : 0,
			"top" : $("#container").offset().top + (getElSize(50)),
			"color" : "white",
			"right" : marginWidth + getElSize(50), 
			"font-weight" : "bolder",
			"font-size" : getElSize(40)
		});
		
		$("#tableDiv").css({
			"left" : $("#container").offset().left + getElSize(20),
			"width" : getElSize(600),
			"top" : $("#container").offset().top + getElSize(450)
		});
		
		//$("#table").css("margin-top", contentHeight/(targetHeight/50));
		$("#tableTitle").css("font-size", getElSize(40));
		$("#tableSubTitle").css("font-size", getElSize(30));
		$(".tr").css("font-size", getElSize(30));
		$(".td").css("padding", getElSize(10));
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(120),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(120),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("#Legend").css({
			"font-size" : getElSize(35),
			"top" : marginHeight + getElSize(600),
			"left" : marginWidth + getElSize(100),
			"color" : "white"
 		});
		
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : 0,
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : -1,
			"background-color" : "black",
			"opacity" : 0.4
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#sDate").css({
			"position" : "absolute",
			"z-index" : 10,
			"font-size" : getElSize(20),
			"left" : originWidth/2 - ($("#container").width()/2) + getElSize(200),
			"top" :$("#tableTitle").offset().top - getElSize(100),
			"opacity"  : 0,
			"width" : getElSize(400)
		});
		
		$("#date_controller button").css({
			"font-size" : getElSize(30),
			"width" : getElSize(60),
			"height" : getElSize(50)
		});
		
		$("#date_controller").css({
			"opacity"  : 0,
			"position" : "absolute",
		});

		$("#date_controller").css({
			"position" : "absolute",
			"left" : $("#sDate").offset().left - $("#date_controller").width(),
			"top" : $("#sDate").offset().top
		});
		
		$("#sDate").css("width",getElSize(300))
		
		$("#excel").css({
			"width" : getElSize(70),
			"cursor" : "pointer"
		});
		
		$("#table_toggle_btn").css({
			"position" : "absolute",
			"left" : marginWidth + getElSize(150),
			"top" : getElSize(300)
		});
	};
	
	var border_interval = null;
	function bodyNeonEffect(color) {
		var lineWidth = getElSize(100);
		var toggle = true;
		border_interval = setInterval(function() {
			$("#container").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= getElSize(3);
				if (lineWidth <= 0) {
					toggle = false;
				};
			} else if (!toggle) {
				lineWidth += getElSize(3);
				if (lineWidth >= getElSize(100)) {
					toggle = true;
				};
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	var csvData = "";
	function csvSend(){
		var sDate, eDate;
		var csvOutput;
		
		sDate = $("#sDate").val();
		eDate = "";
		csvOutput = csvData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
</script>
</head>
<body oncontextmenu="return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<div id="corver"></div>	
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">车间布局</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">设备别运转实绩分析</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">设备报警明细查询</td>
				</tr>
				<tr>
					<td id="menu3" class="menu">夜间无人运转现况</td>
				</tr>
				<tr>
					<td id="menu4" class="menu">无人加工设备运转现况</td>
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<div id="Legend" style="position: absolute; font-size: 50px;">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> <font style="color: white">运转</font><br><br> 
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> <font style="color: white">待机</font><br> <br>
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> <font style="color: white">报警</font> <br><br>
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> <font style="color: white">断电</font>  
	</div>

	<!-- <div id="pieChart1"  ></div> -->
	<div id="pieChart2" ></div>
	
	<div id="date_controller">
		<button id="up">⬆︎</button><button id="down">⬇︎</button>
	</div>
				
	<input type="date" id="sDate">
	<button id="table_toggle_btn">夜间无人运转现况</button>
	<table id="tableDiv" >
		<tr>
			<td align="center"> 
				<font style="font-size: 40px;color: white;font-weight: bold;" id="tableTitle">夜间无人运转现况 (DMM)</font><br>
				<font style="font-size: 40px;color: white;font-weight: bold;" id="tableSubTitle">基准时间 : 18时 ~ 07时</font>
			</td>
			<td align="left">
				<%-- <img alt="" src="${ctxPath }/images/excel.svg" id="excel"> --%>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table style="width: 100%; text-align: center; color: white;" id="table"></table>			
			</td>
		</tr>
	</table>

	<img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>
	<div id="title_right" class="title">斗山机床</div>	

	<font id="date"></font>
	<font id="time"></font>
</body>
</html>