<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
var jig = window.localStorage.getItem("jig");

var shopId = 2;
var dvc = replaceHyphen("${dvcName}");
var wc = "${WC}";
var sDate = "${sDate}";
var eDate = "${eDate}";

function replaceHyphen(str){
	return str.replace(/-/gi,"#");
};

$(function(){
	$(".date").change(getDvcList);
	
	$("#wcSelector").html(wc);
	getWcList();
	setEl();
	setDate();
	//getMousePos();
	
	$("#wcSelector").click(showwcList);
	
	$("#wcList tr").click(function(){
		var jig = $(this).text();
		$("#wcSelector").html(jig)
		clearMenu()
	});
	

	$("#menu_btn").click(function(){
		if(!panel){
			showPanel();
		}else{
			closePanel();
		};
		panel = !panel;
	});
	$(".menu").click(goReport);
	$("#dvcSelector").click(dvcList);
	
	//getWcDataList();
	getDvcList();
	
	$("#table").click(function(){
		location.href = "${ctxPath}/chart/performanceReport.do";
	});
});

function dvcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#dvcList").toggle();	
};

function getDvcList(){
	var url = "${ctxPath}/chart/getJigList4Report.do";
	var wc = $("#wcSelector").html();
	var param = "WC=" + wc + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.dvcList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='dvcTr'>" + 
							"<td >" + decodeURIComponent(data.name) + "</td>" + 
					"</tr>"
			});
			
			$("#dvcList").html(tr);
			$("#dvcList td").css({
				"border" : "1px solid white"
			});
		
			if(!first_load){
				dvc = $("#dvcList td:nth(0)").html();
			};

			first_load = false;	
			$("#dvcSelector").html(dvc);
			
			var sDate = $("#sDate").val();
			var eDate = $("#eDate").val();
		
			showWcDatabyDvc(dvc, sDate, eDate);
			$("#dvcList tr").click(function(){
				clearMenu();
				var dvc = $(this).text();
				$("#dvcSelector").html(dvc);
				showWcDatabyDvc(dvc, sDate, eDate);
			});
			
		}
	});
};

var first_load = true;
function showWcDatabyDvc(dvc, sDate, eDate, ty){
	var url = "${ctxPath}/chart/getWcDataByDvc.do";
	var param = "name=" + dvc + 
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId;

	//$("#wcSelector").html(wc);
	
	if(ty=="jig"){
		$("#wc_sdate").val($("#jig_sdate").val());
		$("#wc_edate").val($("#jig_edate").val());		
	};
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			var tr = "";

			wcList = new Array();
			var wc = new Array();
			
			wc.push("区分");
			wc.push("运转时间");
			wc.push("待机");
			wc.push("报警");
			wc.push("断电");
			
			wcList.push(wc);
			
			dateList = new Array();
			inCycleBar = new Array();
			waitBar = new Array();
			alarmBar = new Array();
			noConnBar = new Array();
			$(json).each(function(idx, data){
				dateList.push(removeHypen(data.workDate.substr(5)));
				tmpArray = dateList;
				
				var wc = new Array();
				wc.push(removeHypen(data.workDate.substr(5)));
				wc.push(data.inCycle_time);
				wc.push(data.wait_time);
				wc.push(data.alarm_time);
				wc.push(Number(Number(24 * 60 * 60 - (data.inCycle_time + data.wait_time + data.alarm_time )).toFixed(1)));
				
				wcList.push(wc);
				tmpWcList = wcList;
				
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				inCycleBar.push(incycle);
				waitBar.push(wait);
				alarmBar.push(alarm);
				noConnBar.push(noconn);
				
				tmpInCycleBar = inCycleBar;
				tmpWaitBar = waitBar;
				tmpAlarmBar = alarmBar;
				tmpNoConnBar = noConnBar;
			});
		
			var blank = maxBar - json.length
			maxPage = json.length - maxBar; 
				
			for(var i = 0; i < blank; i++){
				dateList.push("");
				var wc = new Array();
				wc.push("____");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
				
				inCycleBar.push(0);
				waitBar.push(0);
				alarmBar.push(0);
				noConnBar.push(0);
			};
			
			if(json.length > maxBar){
				overBar = true;
				reArrangeArray();
			};
			
			$(".chartTable").remove();
			var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			
			for(var i = 0; i < wcList[0].length; i++){
				table += "<tr class='contentTr'>";
				for(var j = 0; j < wcList.length; j++){
					if(j==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
					}else if(j==0 || i==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
					}else {
						var n;
						if(typeof(wcList[j][i])=="number"){
							n = Number(wcList[j][i]/60/60).toFixed(1)
						}else{
							n = "";
						};
						table += "<td>" + n + "</td>";
					};
				};
				table += "</tr>";
			};
			
			table += "</table>";
			$("#tableContainer").append(table)
			
			setEl();
			chart("chart");
			addSeries();
			
			//$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
		}
	});
};

var maxBar = 10;
var tmpArray = new Array();
var tmpInCycleBar = new Array();
var tmpWaitBar = new Array();
var tmpAlarmBar = new Array();
var tmpNoConnBar = new Array();
var tmpWcName = new Array();
var tmpWcList = new Array();

function reArrangeArray(){
	dateList = new Array();
	inCycleBar = new Array();
	waitBar = new Array();
	alarmBar = new Array();
	noConnBar = new Array();
	wcName = new Array();
	wcList = new Array();
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		dateList[i-cBarPoint] = tmpArray[i];	
	};
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		inCycleBar[i-cBarPoint] = tmpInCycleBar[i];	
	};
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		waitBar[i-cBarPoint] = tmpWaitBar[i];	
	};
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		alarmBar[i-cBarPoint] = tmpAlarmBar[i];	
	};
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		noConnBar[i-cBarPoint] = tmpNoConnBar[i];	
	};
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		wcName[i-cBarPoint] = tmpWcName[i];	
	};
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
		wcList[i-cBarPoint] = tmpWcList[i];	
	};
	
	var wc = new Array();
	wc.push("区分");
	wc.push("运转时间");
	wc.push("待机");
	wc.push("报警");
	wc.push("断电");
	
	//wcList.push(wc);
	wcList[0] = wc;
};

function nextBarArray(){
	if(cBarPoint>=maxPage || maxPage<=0){
		alert("一览表的结尾");
		return;
	};
	cBarPoint++;
	reArrangeArray();
	
	$(".chartTable").remove();
	var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
	
	for(var i = 0; i < wcList[0].length; i++){
		table += "<tr class='contentTr'>";
		for(var j = 0; j < wcList.length; j++){
			if(j==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
			}else if(j==0 || i==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
			}else {
				var n;
				if(typeof(wcList[j][i])=="number"){
					n = Number(wcList[j][i]/60/60).toFixed(1)
				}else{
					n = "";
				};
				table += "<td>" + n + "</td>";
			};
		};
		table += "</tr>";
	};
	
	table += "</table>";
	$("#tableContainer").append(table)
	
	setEl();
	chart("chart");
	addSeries();
};

function prevBarArray(){
	if(cBarPoint<=0){
		alert("一览表的开头");
		return;
	};
	cBarPoint--;
	reArrangeArray();
	
	$(".chartTable").remove();
	var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
	
	for(var i = 0; i < wcList[0].length; i++){
		table += "<tr class='contentTr'>";
		for(var j = 0; j < wcList.length; j++){
			if(j==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
			}else if(j==0 || i==0){
				table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
			}else {
				var n;
				if(typeof(wcList[j][i])=="number"){
					n = Number(wcList[j][i]/60/60).toFixed(1)
				}else{
					n = "";
				};
				table += "<td>" + n + "</td>";
			};
		};
		table += "</tr>";
	};
	
	table += "</table>";
	$("#tableContainer").append(table)
	
	setEl();
	chart("chart");
	addSeries();
};
var maxPage;
var cBarPoint = 0;
var overBar = false;
function getWcList(){
	var url = "${ctxPath}/chart/getWcList.do";
	var param = "jig=" + jig + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.wcList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr>" + 
							"<td>" + decodeURIComponent(data.WC) + "</td>" + 
					"</tr>"
			});
			
			$("#wcList").html(tr);
			$("#wcList td").css({
				"border" : "1px solid white"
			});
			
			$("#wcList tr").click(function(){
				var wc = $(this).text();
				$("#wcSelector").html(wc.trim())
				clearMenu();
				//getWcDataList();
				getDvcList();
				
				$("#dvcSelector").html("选择")
			});
		}
	});
};
var panel = false;
var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	//$(".date").val(year + "-" + month + "-" + day);
	$("#sDate").val(sDate);
	$("#eDate").val(eDate);
};

function showwcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
		
	});
	
	$("#wcList").toggle();	
};

function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#wcList").css("display","none");
	$("#dvcList").css("display","none");
};

function getDateDiff(date1,date2){
    var arrDate1 = date1.split("-");
    var getDate1 = new Date(parseInt(arrDate1[0]),parseInt(arrDate1[1])-1,parseInt(arrDate1[2]));
    var arrDate2 = date2.split("-");
    var getDate2 = new Date(parseInt(arrDate2[0]),parseInt(arrDate2[1])-1,parseInt(arrDate2[2]));
    
    var getDiffTime = getDate1.getTime() - getDate2.getTime();
    
    return Math.floor(getDiffTime / (1000 * 60 * 60 * 24));
};

var xAxis = new Array();
var barChart;
function chart(id){
	var sDate = window.localStorage.getItem("sDate");
	var eDate = window.localStorage.getItem("eDate");
	
	var date = new Date(sDate);
	
	var margin = (originWidth*0.05)/2;
	$("#" + id).css({
		//"position" : "absolute",
		"margin-left" : $("#tableContainer table td:nth(1)").offset().left - margin - getElSize(80),
		"width" : $("#tableContainer table").width() - $("#tableContainer table td:nth(0)").width()-getElSize(20) + getElSize(80),
		"top" : $(".label").offset().top + $(".label").height() + getElSize(50)
	})
	
	
	$('#' + id).highcharts({
        chart: {
            type: 'column',
            backgroundColor : 'rgb(50, 50, 50)',
            height : contentHeight * 0.5,
            marginLeft:getElSize(80),
            marginRight:0
        },
        title: {
            text:false
        },
        xAxis: {
            categories: dateList,
            labels : {
            	style : {
            		"color" : "white",
            		"font-size" : getElSize(30)
            	}
            }
        },
        yAxis: {
            min: 0,
            max : 24,
            title: {
                text: false
            },
            labels : {
            	style : {
            		"color" : "white",
            		"font-size" : getElSize(30)
            	},
            	enabled:true
            },
            //reversed:true 
        },
        tooltip: {
            enabled : false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
            },
            series : {
            	pointWidth:getElSize(100)
            }
        },
        credits : false,
        exporting : false,
        legend:{
        	enabled : false
        },
        series: []
    });
	
	barChart = $("#" + id).highcharts();
}
function setEl() {
	$(".container").css({
		"width": originWidth,
		"height" : originHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});


	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 2,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$("#panel_table td").addClass("unSelected_menu");

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	$("#wcList").css({
		"position" : "absolute",
		"left" : originWidth/2 + $("#wcList").width()/2,
		"top" : originHeight/2 - $("#wcList").height()/2,
		"z-index" : 9999999,
		"display" : "none"
	}); 
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : getElSize(20),
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 12,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$("#panel_table td").addClass("unSelected_menu");
	
	$("#wcSelector, .goGraph, .excel, .label, #dvcSelector, #table").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
		//"border": getElSize(5) + "px solid gray"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(40)
	});
	
	$(".chartTable").css({
		"margin-top" : getElSize(60),
		"font-size" : getElSize(50),
		//"padding" : getElSize(20),
		"border-collapse" : "collapse"
	});
	
	$(".chartTable td").css({
		"border" : "solid 1px gray"
	});
	
	$("#wcList").css({
		"width" : getElSize(170),
		"position" : "absolute",
		"background-color": "rgb(34,34,34)",
		"left" : $("#wcSelector").offset().left,
		"top" : $("#wcSelector").offset().top + $("#wcSelector").height() + getElSize(50),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	}); 
	
	$("#dvcList").css({
		"width" : getElSize(230),
		"position" : "absolute",
		"background-color": "rgb(34,34,34)",
		"left" : $("#dvcSelector").offset().left,
		"top" : $("#dvcSelector").offset().top + getElSize(70),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$("#chart").css({
		"margin-bottom" : getElSize(20)
	});
	
	$("#arrow_left, #arrow_right").css({
		"width" : getElSize(100),
		"margin-left" : getElSize(50),
		"margin-right" : getElSize(50),
	})
};

function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

var inCycleBar = new Array();
var waitBar = new Array();
var alarmBar = new Array();
var noConnBar = new Array();
var dateList = new Array();
/* function getWcDataList(){
	var url = "${ctxPath}/chart/getWcData.do";

	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	var wc = $("#wcSelector").html();
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&WC=" + wc;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			var tr = "";

			wcList = new Array();
			var wc = new Array();
			
			wc.push("구분");
			wc.push("가동시간");
			wc.push("대기");
			wc.push("중단");
			wc.push("전원 Off");
			
			wcList.push(wc);
			
			inCycleBar = new Array();
			dateList = new Array();
			waitBar = new Array();
			alarmBar = new Array();
			noConnBar = new Array();
			wcName = new Array();
			$(json).each(function(idx, data){
				dateList.push(removeHypen(data.workDate.substr(5)));
					 
				var wc = new Array();
				wc.push(removeHypen(data.workDate.substr(5)));
				wc.push(data.inCycle_time);
				wc.push(data.wait_time);
				wc.push(data.alarm_time);
				wc.push(data.noConnTime);
				
				wcList.push(wc);
				
				inCycleBar.push(Number(Number(data.inCycle_time/60/6).toFixed(1)));
				waitBar.push(Number(Number(data.wait_time/60/60).toFixed(1)));
				alarmBar.push(Number(Number(data.alarm_time/60/60).toFixed(1)));
				noConnBar.push(Number(Number(data.noConnTime/60/60).toFixed(1)));
			});
		
			$(".chartTable").remove();
			var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			
			for(var i = 0; i < wcList[0].length; i++){
				table += "<tr class='contentTr'>";
				for(var j = 0; j < wcList.length; j++){
					if(j==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" +getElSize(200) + "'>" + wcList[j][i] + "</td>";
					}else if(j==0 || i==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34);' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
					}else {
						table += "<td>" + Number(wcList[j][i]/60/60).toFixed(1) + "</td>";
					};
				};
				table += "</tr>";
			};
			
			table += "</table>";
			$("#tableContainer").append(table)
			
			chart("chart");
			addSeries();
			setEl();
			$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
		}
	});
}; */

function removeHypen(str){
	return str.replace(/-/gi,"");
};

function addSeries(){
	/* barChart.addSeries({
		color : "gray",
		data : noConnBar
	}, true); */

	barChart.addSeries({
		color : "red",
		data : alarmBar
	}, true);
	
	barChart.addSeries({
		color : "yellow",
		data : waitBar
	}, true);
	
	barChart.addSeries({
		color : "green",
		data : inCycleBar
	}, true);
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/DIMM.do";
		location.href = url;
	}else if(type=="menu4"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}
};
</script>
</head>

<body oncontextmenu="return false">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" >
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title">斗山机床</div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr>
				<td id="menu0" class="menu">车间布局</td>
			</tr>
			<tr>
				<td id="menu1" class="menu">设备别运转实绩分析</td>
			</tr>
			<tr>
				<td id="menu2" class="menu">设备报警明细查询</td>
			</tr>
			<tr>
				<td id="menu3" class="menu">夜间无人运转现况</td>
			</tr>
			<tr>
				<td id="menu4" class="menu">无人加工设备运转现况</td>
			</tr>
		</table>
	</div>
	<img src="${ctxPath }/images/menu.png" id="menu_btn" >
	
	<table id="wcList" style="color:white; border-collapse: collapse;"></table>
	<table id="dvcList" style="color:white; border-collapse: collapse;"></table>
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
			<center>
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
								设备别运转实绩分析
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%" class="label">
						WC <span style="background-color: white; color:black; font-weight: bolder;" id="wcSelector">选择</span>
						设备名<span style="background-color: white; color:black; font-weight: bolder;" id="dvcSelector">选择</span> 
						运转时间 <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate"> 
						<span style="background-color: white; color:black; font-weight: bolder;" id="table">表</span>
				</div>
				
				<div id="chartContainer" style="width: 95%">
					<div id="chart" style="width: 100%">
						
					</div>
					<img alt="" src="${ctxPath }/images/arrow_left.png" id="arrow_left"  onclick="prevBarArray();">
					<img alt="" src="${ctxPath }/images/arrow_right.png" id="arrow_right" onclick="nextBarArray();">
					<div id="tableContainer" style="width: 100%"></div>
				</div>				
			</center>
		</div>
	</div>
	
</body>
</html>