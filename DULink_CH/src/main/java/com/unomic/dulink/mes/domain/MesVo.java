package com.unomic.dulink.mes.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class MesVo{
	String AUFNR;
	String MATNR;
	String MAKTX;
	String BDTER;
	String EQUNR;
	String EMONU;

}
