package com.unomic.dulink.mobile.domain;

import java.util.ArrayList;
import java.util.List;



import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class MobileVo{
	private static int ALARMLIMIT = 2;
	
	String mainProgName;
	String ncStatus;
	String ncMode;
	String date;
	String time;
	String color;
	String feedOverride;
	
	String spindleLoad;
	String alarmNumber1;
	String alarmMsg1;
	String alarmType1;
	String alarmMtesAction1;

	String alarmNumber2;
	String alarmMsg2;
	String alarmType2;
	String alarmMtesAction2;
	String localIp;
	
	String MD5;
	
	Long timeStamp;
	
}
