package com.unomic.dulink.scheduler.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.service.AdapterService;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.service.DeviceService;
import com.unomic.dulink.sensor.service.SensorService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/scheduler")
@Controller
public class SchedulerController {

	private static final Logger logger = LoggerFactory.getLogger(SchedulerController.class);
	
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private AdapterService adapterService;

	@Autowired
	SensorService sensorService;

	@Scheduled(fixedDelay = 180000)
	public void samplingData(){
		//Go to DB batch.
		//adjustDeviceStatus(); 
		//Change to whole stauts times.

		//Change to OpPf
		//calcDeviceTimes();

		DeviceVo inputVo = new DeviceVo();
		inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcOpPf(inputVo);
		inputVo.setTargetDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcNight(inputVo);
		
		calcAWListEndTime();
	}

	@Scheduled(cron="0 */30 * * * * ") 
	public void addTemperValue() throws Exception{
		sensorService.addListTemper(sensorService.parseListTemper());
	}

   @Scheduled(fixedDelay = 10000)
	public void runPolling(){
		//getAgentData();
		getIOLdata();
	}

	public void getIOLdata(){
		adapterService.getIOLData();
	}
	
	public void calcAWListEndTime(){
		deviceService.calcAWListEndTime();
	}
	
	public String calcDeviceTimes(){
		DeviceVo setVo = new DeviceVo();
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		deviceService.calcDeviceTimes(setVo);
		return "OK";
	}

	public void calcOpPf(DeviceVo inputVo){
		deviceService.calcDvcOpPf(inputVo);
	}
	
	public void calcNight(DeviceVo inputVo){
		deviceService.calcNight(inputVo);
		deviceService.calcNight_8(inputVo);
	}

}

