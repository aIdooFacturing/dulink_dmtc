package com.unomic.dulink.team.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unomic.dulink.staff.domain.StaffVo;
import com.unomic.dulink.staff.service.StaffService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/team")
@Controller
public class TeamController {
	
	private static final Logger logger = LoggerFactory.getLogger(TeamController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private StaffService staffService;
	
	
}
