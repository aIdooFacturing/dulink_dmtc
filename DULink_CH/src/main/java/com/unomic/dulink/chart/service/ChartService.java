package com.unomic.dulink.chart.service;

import java.util.List;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.TimeChartVo;

public interface ChartService {
	public String getStatusData(ChartVo chartVo) throws Exception;
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception;
	public void addData() throws Exception;
	public String getDvcName(ChartVo chartVo) throws Exception;
	public String getAllDvcStatus(ChartVo chartVo) throws Exception;
	public String getAllDvcId(ChartVo chartVo)throws Exception;
	public String getBarChartDvcId(ChartVo chartVo) throws Exception;
	public String polling1()throws Exception;
	public String setVideo(String id) throws Exception;
	public void initVideoPolling() throws Exception;
	public void initPiePolling() throws Exception;
	public String piePolling1() throws Exception;
	public String piePolling2() throws Exception;
	public String setChart1(String id) throws Exception;
	public List getWC()throws Exception;
	public String setChart2(String id) throws Exception;
	public void setWC(ChartVo chartVo) throws Exception;
	public String getStartTime() throws Exception;
	public void delVideoMachine(String id) throws Exception;
	public void delChartMachine(String id) throws Exception;
	public String getAdapterId(ChartVo chartVo)throws Exception;
	public String showDetailEvent(ChartVo chartVo)throws Exception;
	public void test(ChartVo chartVo) throws Exception;
	public String getDetailStatus(ChartVo chartVo) throws Exception;
	public String getAlarmList(ChartVo chartVo) throws Exception;
	public List <TimeChartVo> getTimeChartData(ChartVo chartVo);

	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception;
	public List<ChartVo> testTimeChartData(ChartVo inputVo);
	public String getJicList(ChartVo chartVo) throws Exception;
	public String getWcList(ChartVo chartVo) throws Exception;
	public String getTableData(ChartVo chartVo)throws Exception;
	public String getWcData(ChartVo chartVo) throws Exception;
	public String getWcDataByDvc(ChartVo chartVo) throws Exception;
	public String getDvcList(ChartVo chartVo) throws Exception;
	public String getAlarmData(ChartVo chartVo) throws Exception;
	public String getJigList4Report(ChartVo chartVo) throws Exception;
};
