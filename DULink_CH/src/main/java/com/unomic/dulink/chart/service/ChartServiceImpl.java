package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;


@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";

	Map statusMap = new HashMap();
	int chartOrder = 0;
	public boolean chkOrderStatus(String status){
		boolean result = false;
//		if(chartOrder<=Integer.parseInt((String) statusMap.get(status))){
//			chartOrder = Integer.parseInt((String) statusMap.get(status));
//			result = true;
//		};
	return result;
	};
	
	
	public String addZero(String str){
		if(str.length()==1){
			return "0" + str;
		}else{
			return str;
		}
	};
	
	@Override
	public String getStatusData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "getStatusData", chartVo);
	};

	@Override
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentDvcData", chartVo);
		//chartVo.setEndDateTime((String)sql.selectOne(namespace + "getLastUpdateTime", chartVo));
		 
		if(chartVo==null){
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		};

		return chartVo;
	};

	@Override
	public void addData() throws Exception {
		SqlSession sql = getSqlSession();
		ChartVo chartVo = new ChartVo();
		
		for(int i = 0; i <=1 ; i++){
			String status = "";
			int random = (int) (Math.random()*10+1) + (int) (Math.random()*10+1) + (int) (Math.random()*10+1) + (int) (Math.random()*10+1);
			if(random==40){
				status = "Alarm";
			}else if(random>=38){
				status = "Wait";
			}else{
				status = "In-Cycle";
			};
			
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String date = sdf.format(d);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = sdf2.format(d);
			
			String spdLoad = String.valueOf(Math.floor((Math.random()*10+1) + (Math.random()*10+1)+ (Math.random()*10+1) + 60)).substring(0,2);
			if(spdLoad.equals("90")) spdLoad = "0";
			String feedOverride = String.valueOf(Math.floor((Math.random()*10+1) + (Math.random()*10+1) + (Math.random()*10+1) + 60)).substring(0,2); 
			if(feedOverride.equals("90")) spdLoad = "0";
			
			int dvcId = 0, adtId = 0;
			if(i==0){
				dvcId = 1;
				adtId = 13;
			}else if(i==1){
				dvcId = 5;
				adtId = 14;
			}
			
			chartVo.setDvcId(Integer.toString(dvcId));
			chartVo.setLastUpdateTime(time);
			chartVo.setStartDateTime(time);
			chartVo.setEndDateTime(time);
			chartVo.setAdtId(Integer.toString(adtId));
			chartVo.setSpdLoad(spdLoad);
			chartVo.setChartStatus(status);
			chartVo.setFeedOverride(feedOverride);
			chartVo.setDate(date);
			
			String chartStatus = (String) sql.selectOne(namespace + "getLastChartStatus", chartVo);
			
			if(chartStatus.equals(status)){
				sql.update(namespace + "updateDeviceStatus", chartVo);
			}else{
				sql.update(namespace + "updateDeviceStatus", chartVo);
				chartVo.setStartDateTime((String) sql.selectOne(namespace + "getStartDateTime", chartVo));
				sql.update(namespace + "setChartEndTime", chartVo);
				chartVo.setStartDateTime(time);
				sql.insert(namespace + "addChartStatus", chartVo);
			}
		};
	}

	@Override
	public String getDvcName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String dvcName = (String) sql.selectOne(namespace + "getDvcName", chartVo);
		return dvcName;
	}

	@Override
	public String getAllDvcStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> chartData = sql.selectList(namespace + "getAllDvcStatus", chartVo);
		List list = new ArrayList();
		for(int i = 0; i < chartData.size(); i++){
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("spdActualSpeed", chartData.get(i).getSpdActualSpeed());
			map.put("feedOverride", chartData.get(i).getFeedOverride());
			map.put("actualFeed", chartData.get(i).getActualFeed()); 
			map.put("date", chartData.get(i).getDate());
			map.put("name", chartData.get(i).getName());
			map.put("dvcId", chartData.get(i).getDvcId());
			
			list.add(map);
		};
		
		String str = "";
		
		Map resultMap = new HashMap();
		resultMap.put("chartStatus", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getAllDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcIdList = sql.selectList(namespace + "getAllDvcId", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < dvcIdList.size();i++){
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("adtId", dvcIdList.get(i).getAdtId());
			
			String jig = "";
			if(dvcIdList.get(i).getJig()!=null){
				jig = URLEncoder.encode(dvcIdList.get(i).getJig(), "UTF-8");
				jig = jig.replaceAll("\\+", "%20");
			};
			
			String wc = "";
			if(dvcIdList.get(i).getWC()!=null){
				wc = URLEncoder.encode(dvcIdList.get(i).getWC(), "UTF-8");
				wc = wc.replaceAll("\\+", "%20");
			};
			
			map.put("jig", jig);
			map.put("WC", wc);

			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		
		return str;
	}
	
	@Override
	public String polling1() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "getVideoMachine");
	};

	@Override
	public String setVideo(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";
		
		try{
			sql.update(namespace + "setVideo", id);
			rtn = "success";
		}catch(Exception e){
			rtn = "fail";
		};
		return rtn;
	};

	@Override
	public void initVideoPolling() throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "initVideoPolling");
	};

	@Override
	public void initPiePolling() throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "initPiePolling");
	}

	@Override
	public String piePolling1() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "piePolling1");
	};

	@Override
	public String piePolling2() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "piePolling2");
	};

	@Override
	public String setChart1(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";
		
		try{
			sql.update(namespace +"setChart1", id);
			rtn = "success";
		}catch(Exception e){
			rtn = "fail";
		}
		return rtn;
	};

	@Override
	public String setChart2(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";
		
		try{
			sql.update(namespace +"setChart2", id);
			rtn = "success";
		}catch(Exception e){
			rtn = "fail";
		}
		return rtn;
	}

	@Override
	public void delVideoMachine(String id) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace +"delVideoMachine", id);
	}

	@Override
	public void delChartMachine(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String machine = (String) sql.selectOne(namespace + "chkChartMachine1", id);

		if(machine==null){
			sql.update(namespace +"delChartMachine2");
		}else{
			sql.update(namespace +"delChartMachine1");
		};
	}

	@Override
	public void test(ChartVo chartVo) throws Exception {

		SqlSession sql = getSqlSession();
		List <ChartVo> chartData = sql.selectList(namespace + "getAllDvcStatus", chartVo);
	}

	@Override
	public String getAdapterId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcIdList = sql.selectList(namespace + "getAdapterId", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < dvcIdList.size();i++){
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("adtId", dvcIdList.get(i).getAdtId());

			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public String getBarChartDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcIdList = sql.selectList(namespace + "getBarChartDvcId", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < dvcIdList.size();i++){
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("jig", URLEncoder.encode(dvcIdList.get(i).getJig(), "UTF-8"));
			map.put("WC", dvcIdList.get(i).getWC());
			map.put("chartOrder", dvcIdList.get(i).getChartOrder());
			map.put("adtId", dvcIdList.get(i).getAdtId());
			
			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		
		return str;
	}


	@Override
	public String showDetailEvent(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> chartData  = sql.selectList(namespace +"showDetailEvent", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < chartData.size(); i++){
			Map map = new HashMap();
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("feedOverride", chartData.get(i).getFeedOverride());
			
			list.add(map);
		};
		
		String str = "";
		Map resultMap = new HashMap();
		resultMap.put("chartData", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}


	@Override
	public String getDetailStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> chartData = sql.selectList(namespace + "getDetailChart", chartVo);
		
		List statusList = new ArrayList();
		for(int i = 0; i < chartData.size(); i++){
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			
			statusList.add(map);
		};
		
		Map map = new HashMap();
		map.put("chartStatus", statusList);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		
		str = om.defaultPrettyPrintingWriter().writeValueAsString(map);
		System.out.println(str);
		return str;
	}


	@Override
	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getOtherChartsData", chartVo);
		return chartVo;
	};
	
	
	
	//로직 구성. 1부터 144까지 빈칸 만들기.
	//default no-connection
	//현재시간 10분 전까지 계산.
	//같은 시간 있으면 넣기.
	// 들어간 날짜로 조회하는 루틴 필요.
	@Override
	public List <TimeChartVo> getTimeChartData(ChartVo inputVo){
		SqlSession sql = getSqlSession();	
		List <ChartVo> listChartData = sql.selectList(namespace + "getTimeChartData", inputVo);
		
		if(listChartData.size()<1){
			return null;
		}
		
		List <TimeChartVo> tmpTimeChart = new ArrayList();
		int CHART_SIZE = 144;
		
		String stTime = CommonFunction.getChartStartTime(inputVo.getTargetDateTime());
		Long unixTime = CommonFunction.dateTime2Mil(stTime);
		String stDate = stTime;
		
		//10분 추가.
		String edDate = CommonFunction.unixTime2Datetime(CommonFunction.dateTime2Mil(stDate)+600000L);
		for(int i=0 ; i < CHART_SIZE; i++){

			TimeChartVo tmpVo = new TimeChartVo();
			tmpVo.setStartTime(stDate);
			tmpVo.setEndTime(edDate);
			tmpVo.setY(CommonCode.MSG_CHART_DIST);
			
			Iterator<ChartVo>ite = listChartData.iterator();
			tmpVo.setColor(CommonCode.MSG_COLOR_NO_CONNECTION);
			while(ite.hasNext()){
				ChartVo tmpChartVo = ite.next();
				if( CommonFunction.cutRight(tmpChartVo.getStartDateTime(),2).equals(tmpVo.getStartTime() )){
					if(tmpChartVo.getChartStatus().equals(CommonCode.MSG_ALARM)){
						tmpVo.setColor(CommonCode.MSG_COLOR_ALARM);
					}else if(tmpChartVo.getChartStatus().equals(CommonCode.MSG_WAIT)){
						tmpVo.setColor(CommonCode.MSG_COLOR_WAIT);
					}else if(tmpChartVo.getChartStatus().equals(CommonCode.MSG_IN_CYCLE)){
						tmpVo.setColor(CommonCode.MSG_COLOR_IN_CYCLE);
					}
				}
				
				// 가져온 마지막 데이터일때... loop 탈출.
				if(! ite.hasNext()
				  && CommonFunction.cutRight(tmpChartVo.getStartDateTime(),2).equals(tmpVo.getStartTime() )){
					tmpTimeChart.add(tmpVo);

					return tmpTimeChart;
					
				}
			}
			stDate = edDate;
			edDate = CommonFunction.unixTime2Datetime(CommonFunction.dateTime2Mil(stDate)+600000L);
			tmpTimeChart.add(tmpVo);
		}
		return tmpTimeChart;
	};
	
	@Override
	public List<ChartVo> testTimeChartData(ChartVo inputVo){
		SqlSession sql = getSqlSession();	
		List <ChartVo> listChartData = sql.selectList(namespace + "getTimeChartData", inputVo);
		
		return listChartData;
	}


	@Override
	public String getJicList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> jigList  = sql.selectList(namespace + "getJicList", chartVo);
		
		List jigs = new ArrayList();
		for(int i = 0; i < jigList.size(); i++){
			Map map = new HashMap();
			map.put("jig",  URLEncoder.encode(jigList.get(i).getJig(),"utf-8"));
			
			jigs.add(map);
		};
		
		Map jigMap = new HashMap();
		jigMap.put("jigList", jigs);
		
		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		return str;
	};
	
	@Override
	public String getWcList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> jigList  = sql.selectList(namespace + "getWcList", chartVo);
		
		List jigs = new ArrayList();
		for(int i = 0; i < jigList.size(); i++){
			Map map = new HashMap();
			map.put("WC",  URLEncoder.encode(jigList.get(i).getWC(),"utf-8"));
			
			jigs.add(map);
		};
		
		Map jigMap = new HashMap();
		jigMap.put("wcList", jigs);
		
		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		return str;
	}


	@Override
	public String getTableData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		System.out.println(chartVo);
		List <ChartVo> dataList = sql.selectList(namespace + "getDataList", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("WC", dataList.get(i).getWC());
			map.put("name", dataList.get(i).getName());
			map.put("target_time", dataList.get(i).getTargetDateTime());
			map.put("inCycle_time", dataList.get(i).getInCycleTime());
			map.put("wait_time", dataList.get(i).getWaitTime());
			map.put("alarm_time", dataList.get(i).getAlarmTime());
			map.put("noConnTime", dataList.get(i).getNoConnectionTime());
			map.put("GRNM", URLEncoder.encode(dataList.get(i).getGRNM(),"utf-8").replaceAll("\\+", "%20"));
			map.put("name", dataList.get(i).getName());
			
			list.add(map);
		};
		
		Map dataMap = new HashMap();
		dataMap.put("tableData", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	
	@Override
	public String getWcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> wcList = sql.selectList(namespace + "getWcData", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < wcList.size(); i++){
			Map map = new HashMap();
			
			map.put("WC", wcList.get(i).getWC());
			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("GRNM", URLEncoder.encode(wcList.get(i).getGRNM(),"utf-8").replaceAll("\\+", "%20"));
			map.put("workDate", wcList.get(i).getWorkDate());
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("wcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getWcDataByDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> wcList = sql.selectList(namespace + "getWcDataByDvc", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < wcList.size(); i++){
			Map map = new HashMap();
			
			map.put("WC", wcList.get(i).getWC());
			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("GRNM", URLEncoder.encode(wcList.get(i).getGRNM(),"utf-8").replaceAll("\\+", "%20"));
			map.put("workDate", wcList.get(i).getWorkDate());
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("wcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getDvcList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> dvcList = sql.selectList(namespace + "getDvcList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dvcList.size(); i++){
			Map map = new HashMap();
			map.put("wc", dvcList.get(i).getWC());
			
			list.add(map);
		};
		
		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);
		
		return str;
	}


	@Override
	public String getAlarmData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> alarmList = sql.selectList(namespace + "getAlarmData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < alarmList.size(); i++){
			Map map = new HashMap();
			map.put("wc", alarmList.get(i).getWC());
			map.put("GRNM", URLEncoder.encode(alarmList.get(i).getGRNM(),"utf-8").replaceAll("\\+", "%20"));
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			map.put("delayTimeSec", alarmList.get(i).getDelayTimeSec());
			map.put("alarmMsg", URLEncoder.encode(alarmList.get(i).getAlarmMsg(),"utf-8").replaceAll("\\+", "%20"));
			
			list.add(map);
		};
		
		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);

		return str;
	}


	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcList = sql.selectList(namespace + "getJigList4Report", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dvcList.size(); i++){
			Map map = new HashMap();
			map.put("name", dvcList.get(i).getName());
			list.add(map);
		};
		
		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);
		
		return str;
	}


	@Override
	public String getAlarmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> alarmList = sql.selectList(namespace + "getAlarmList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < alarmList.size(); i++){
			Map map = new HashMap();
			map.put("alarmCode", alarmList.get(i).getAlarmCode());
			map.put("alarmMsg", alarmList.get(i).getAlarmMsg());
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			
			list.add(map);
		};
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);
		
		return str;
	}

	@Override
	public String getStartTime() throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime");
		return rtn;
	}


	@Override
	public void setWC(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setWC", chartVo);
		
	}


	@Override
	public List getWC() throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> list = sql.selectList(namespace + "getWC");
		return list;
	};
	
};