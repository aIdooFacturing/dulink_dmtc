package com.unomic.dulink.chart.domain;


import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class ChartVo{
	String adtId;
	String dvcId;
	String delayTimeSec;
	String startDateTime;
	String alarmCode;
	String alarmMsg;
	Integer shopId;
	String targetTime;
	String sDate;
	String eDate;
	String GRNM;
	String workDate;
	String isOld;
	String ty;
	String endDateTime;
	String chartStatus;
	String spdLoad;
	String spdOverride;
	String spdActualSpeed;
	String rapidOverride;
	String actualFeed;
	String feedOverride;
	String programHeader;
	String programName;
	String alarm;
	String date;
	String callback;
	String name;
	String lastUpdateTime;
	String lastModal;
	String operationTime;
	String WC;
	String jig;
	int chartOrder;
	String type;
	int spdTime;
	String lastProgramName;
	String lastProgramHeader;
	String startTime;
	String endTime;
	int cuttingTime;
	int inCycleTime;
	int waitTime;
	int alarmTime;
	int noConnectionTime;
	double opRatio;
	double cuttingRatio;
	
	String targetDateTime;
	
	List<String> nameMap = null;
}
