package com.unomic.dulink.device.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class DeviceVo{
	String adtId;
	String dvcId;
	String adapterId;
	String lastUpdateTime;
	Integer shopId;
	String lastStartDateTime;
	String lastChartStatus;
	Integer lastFeedOverride;
	Float lastSpdLoad;
	String lastAlarm;
	String lastModal;
	String workDate;
	String lastProgramName;
	String lastProgramHeader;
	String stopDate;
	String stopTime;
	
	//for night chart
	String targetDate;
	String nightStart;
	String nightEnd;
	String genStart;
	String genEnd;
	
	String stDate;
	String edDate;
	String isNight;
	String slideSeq;
	
}
