package com.unomic.dulink.device.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.domain.NightChartVo;


@Service
@Repository
public class DeviceServiceImpl extends SqlSessionDaoSupport implements DeviceService{
	private final static String DEVICE_SPACE= "com.dulink.device.";
	private final static String ADAPTER_SPACE= "com.dulink.adapter.";
	
	@Override
	@Transactional
	public String editLastStatus(DeviceVo inputVo)
	{
		SqlSession sqlSession=getSqlSession();
		sqlSession.update(DEVICE_SPACE + "editDvcLastStatus", inputVo);

		return "OK";
	}
	
	@Override
	@Transactional
	public String editLastStatusIOL(DeviceVo inputVo)
	{

		SqlSession sqlSession=getSqlSession();
		sqlSession.update(DEVICE_SPACE + "editDvcLastStatusIOL", inputVo);

		return "OK";
	}
	
	@Override
	@Transactional
	public String adjustDeviceStatus()
	{
		logger.info("@@@@@run adjustDeviceStatus@@@@@");
		SqlSession sqlSession=getSqlSession();
		
		List<DeviceVo> listDvc = sqlSession.selectList(DEVICE_SPACE+"getListADT_NoConnection");
		
		HashMap <String, Object> dataMap = new HashMap<String, Object>();

		if(listDvc.size()==0){
			return "OK";
		}
		
		List<AdapterVo> listPureEdit = new ArrayList<AdapterVo>();		
		Iterator<DeviceVo> itr = listDvc.iterator();
		
		while(itr.hasNext()){
			AdapterVo tmpVo = new AdapterVo();
			DeviceVo tmpDvcVo = itr.next();
			tmpVo.setAdtId(tmpDvcVo.getAdtId());
			tmpVo.setChartStatus(CommonCode.MSG_NO_CONNECTION);
			tmpVo.setStartDateTime(CommonFunction.getTodayDateTime());
			tmpVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
			listPureEdit.add(tmpVo);
		}
		dataMap.put("listPure", listPureEdit);
		
		sqlSession.update(ADAPTER_SPACE + "editAllAdtNoConnection", dataMap);
		sqlSession.insert(ADAPTER_SPACE + "addListAdtNoConnection", dataMap);
		
		sqlSession.update(DEVICE_SPACE + "adjustDeviceStatus");
		return "OK";
	}
	
	@Override
	@Transactional
	public String calcDeviceOptime(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editCalcOptime",dvcVo);
		
		return "OK";
	}

	@Override
	@Transactional
	public String calcAWListEndTime()
	{
		logger.info("@@@@@run editAWList@@@@@");
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editAWList");
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String editSetNightDvc(DeviceVo inputVo)
	{
		logger.info("@@@@@run editSetNightDvc@@@@@");
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editSetNightDvc",inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String editSetSlideSeqDvc(DeviceVo inputVo)
	{
		logger.info("@@@@@run editSetSlideSeqDvc@@@@@");
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editSetSlideSeqDvc",inputVo);
		
		return "OK";
	}
	
	
	
	
	@Override
	@Transactional
	public String calcDeviceTimes(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editCalcOptime",dvcVo);
		sql.update(DEVICE_SPACE + "editCalcIncycleTime",dvcVo);
		sql.update(DEVICE_SPACE + "editCalcWaitTime",dvcVo);
		sql.update(DEVICE_SPACE + "editCalcAlarmTime",dvcVo);
		sql.update(DEVICE_SPACE + "editCalcNoconnectionTime",dvcVo);
		
		//must update after editCalcIncycleTime
		sql.update(DEVICE_SPACE + "editCalcCuttingTime",dvcVo);
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String editDvcHealth(DeviceVo dvcVo)
	{
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editDvcHealth",dvcVo);
		return "OK";
	}
	
	@Override
	public String testProcedure(DeviceVo dvcVo)
	{
		SqlSession sql = getSqlSession();
		dvcVo.setDvcId("22");
		String strSpTest = (String) sql.selectOne(DEVICE_SPACE + "spTest",dvcVo);
		
		logger.error("strSpTest : "+strSpTest);
		
		return "OK";
	}
	
	
	@Override
	@Transactional
	public String addDailyDvcStatus()
	{
		SqlSession sql = getSqlSession();
		sql.insert(DEVICE_SPACE + "addDailyDvcStatus");
		return "OK";
	}
	
	
	/*
	 * @author cannon
	 * 
	 * @param String targetDate 추출 대상 날짜. controller에서 입력.
	*/
	@Override
	@Transactional
	public List<NightChartVo> getNightlyDvcStatus(DeviceVo inputVo)
	{

		SqlSession sql = getSqlSession();
		List< NightChartVo> listRtn = sql.selectList(DEVICE_SPACE + "getNightlyDvcStatus",inputVo);
		
		return listRtn;
	}
	
	@Override
	@Transactional
	public List<NightChartVo> getNightlyDvcStatus_8(DeviceVo inputVo)
	{

		SqlSession sql = getSqlSession();
		List< NightChartVo> listRtn = sql.selectList(DEVICE_SPACE + "getNightlyDvcStatus_8",inputVo);
		
		return listRtn;
	}
	
	
	// count 때려서 체크해보고 있을경우 지우고 새로 하기.? ㅇㅋ.
	@Override
	@Transactional
	public String calcDvcOpPf(DeviceVo inputVo){
		SqlSession sql = getSqlSession();
		
		int cnt = (int) sql.selectOne(DEVICE_SPACE+"cntOpPf", inputVo);
		if(cnt>0){
			sql.delete(DEVICE_SPACE+"rmvOpPf", inputVo);
		}
		
		sql.insert(DEVICE_SPACE+"calcOpPf", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String calcNight(DeviceVo inputVo){
		SqlSession sql = getSqlSession();
		sql.insert(DEVICE_SPACE+"calcNightStatus", inputVo);
		return "OK";
	}
	
	@Override
	@Transactional
	public String calcNight_8(DeviceVo inputVo){
		SqlSession sql = getSqlSession();
		sql.insert(DEVICE_SPACE+"calcNightStatus_8", inputVo);
		return "OK";
	}
	
	
	// 1. 하루 리스트 가져오기??
	// 2. 로직에 따라 비교하기.
	// 3. 비교 후 인설트.
	// 4. 쿼리 수행시간 비교 후 5분단위로 스케쥴링?
	// 예상 row수 하루에 100줄.
	private List<AdapterVo> setAlarmList (AdapterVo preVo, List<AdapterVo> list){

		List<AdapterVo> rtnList = new ArrayList<AdapterVo>(); 
		int size = list.size();
		if(1 > size){
			return null;
		}
		
		AdapterVo innerPreVo = new AdapterVo();
		innerPreVo = preVo;
		for(int i = 0 ; i < size;i++){
			AdapterVo tmpVo = list.get(i);
			
			//현재 상태가 알람이고 이전상태가 알람이나 데이트 스타터가 아닐경우.
			
			if(tmpVo.getChartStatus().equals(CommonCode.MSG_ALARM)
					&& ( !innerPreVo.getChartStatus().equals(CommonCode.MSG_ALARM) && !innerPreVo.getChartStatus().equals(CommonCode.MSG_DATE_START)) ){
				//Alarm List에 삽입. 스타트 시간 삽입.
			}else
			
			//현재상태가 알람이나 데이트 스타터가 아니고 이전 상태가 알람일 경우.
			if( (!tmpVo.getChartStatus().equals(CommonCode.MSG_ALARM) && !tmpVo.getChartStatus().equals(CommonCode.MSG_DATE_START))
				&& innerPreVo.getChartStatus().equals(CommonCode.MSG_ALARM) ){
				//Alarm List에 삽입. 끝나는 시간 삽입???
				//언제 시작된 알람인지 어떻게 알까...???
				//같은 리스트안에 있을수도있음. 어짜피 장비 ID는 같으니 리스트에 endtime없는 알람 있으면 거기에 넣기. 없으면...?
				//테이블 조회해보고 거기에 넣기...???
				//테이블에도 없을 경우 그냥 넣기.
				//일단 그냥 빈 알람 넣어야지 버그 있을경우 고쳐짐.
			}

		}
		// 이렇게 하던지.. 아니면 다른방법..??
		// 5분에 한번씩 처리하려면..?
		// 오늘 아침부터 현재 시간까지 데이터 가져옴.
		// 위에 로직으로 리스트 구분.
		// 마지막 데이터까지 알람이면 해제시간, 조치시간 필요없음. 그외에는 필요함.
		
		return rtnList;
	}
	
	
	@Override
	@Transactional
	public String addChData(){
		SqlSession sql = getSqlSession();
		
		sql.insert(ADAPTER_SPACE+"addAdtData");
		sql.insert(ADAPTER_SPACE+"addDvcData");
		sql.insert(ADAPTER_SPACE+"addMappingData");
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String addChLastData(){
		SqlSession sql = getSqlSession();
		
		sql.insert(ADAPTER_SPACE+"addDvcLastData");
		
		return "OK";
	}

	@Override
	@Transactional
	public List<DeviceVo> getDvcLast(){
		SqlSession sql = getSqlSession();
		List<DeviceVo> rtnList = sql.selectList(DEVICE_SPACE+"getListDvcLast");
		return rtnList;
	}
	
}