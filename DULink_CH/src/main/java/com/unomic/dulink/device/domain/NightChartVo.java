package com.unomic.dulink.device.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class NightChartVo{
	String name;
	
	String stopDate;
	String stopTime;
	String status;
	
	String isOld;
	String inputDate;
}
