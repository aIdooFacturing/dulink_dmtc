package com.unomic.dulink.sensor.service;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.domain.MTConnect;
import com.unomic.dulink.adapter.domain.RogerParser;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.domain.NightChartVo;
import com.unomic.dulink.sensor.domain.SensorVo;
import com.unomic.dulink.sensor.domain.TemperVo;


@Service
@Repository
public class SensorServiceImpl extends SqlSessionDaoSupport implements SensorService{
	private final static String DEVICE_SPACE= "com.dulink.device.";
	private final static String ADAPTER_SPACE= "com.dulink.adapter.";
	private final static String SENSOR_SPACE= "com.dulink.sensor.";
	
	@Override
	public List<TemperVo> parseListTemper() throws Exception{
		String ADT_IP="10.33.74.213:8888";
		String URL="/current";
		
		Document document;

		document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(CommonCode.MSG_HTTP + ADT_IP + URL);

		XPath xpath = XPathFactory.newInstance().newXPath();	
		
		NodeList cols = (NodeList)xpath.evaluate("//Events/Temperature", document, XPathConstants.NODESET);
		
		List<TemperVo> listTemper = new ArrayList<TemperVo>();
		
        for( int idx=0; idx<cols.getLength(); idx++ ){
        	Node node = cols.item(idx);
        	TemperVo tmpVo1= new TemperVo();
        	tmpVo1.setTemperId(node.getAttributes().getNamedItem("dataItemId").getTextContent());
        	tmpVo1.setTemperValue(node.getTextContent());
        	
        	Long statusUnixSec = CommonFunction.mtcDateTime2MilTemper(node.getAttributes().getNamedItem("timestamp").getTextContent());
        	tmpVo1.setTemperDatetime(CommonFunction.unixTime2Datetime(statusUnixSec));
        	//using database time
        	//tmpVo1.setRegdt(CommonFunction.getTodayDateTime());

        	listTemper.add(tmpVo1);
        }
        
        Iterator<TemperVo>  ite = listTemper.iterator();
        
//        System.out.println("iteHasNext:"+ite.hasNext());
//        while(ite.hasNext()){
//        	TemperVo tmpVo2= ite.next();
//        	System.out.println("id:"+tmpVo2.getTemperId());
//        	System.out.println("datetime:"+tmpVo2.getTemperDatetime());
//        	System.out.println("value:"+tmpVo2.getTemperValue());
//        	System.out.println("regdt:"+tmpVo2.getRegdt());
//        }
        
        return listTemper;
	}
	
	@Override
	public String addListTemper(List<TemperVo> inputList){
		SqlSession sql=getSqlSession();
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("listTemper", inputList);
    	
        sql.insert(SENSOR_SPACE + "addListTemper", dataMap);
        return "OK";
	}
	
	@Override
	public List<TemperVo> getListTemperDate(SensorVo inputVo){
		SqlSession sql=getSqlSession();
		List<TemperVo> inputList = sql.selectList(SENSOR_SPACE+"getListTemperDate", inputVo);
		List<TemperVo> rtnList = new ArrayList<TemperVo>();
		Iterator <TemperVo> ite = inputList.iterator();
		while(ite.hasNext()){
			TemperVo tmp = new TemperVo();
			tmp = ite.next();
			tmp.setTemperValue(checkDigit(tmp.getTemperValue()));
			rtnList.add(tmp);
		}
		
        return rtnList;
	}
	
	public String checkDigit(String val){
		String rtn = val;
		String left;
		String right;
		if(val.length()==3){
			left=val.substring(val.length()-3, val.length()-1);
			right=val.substring(val.length()-1, val.length());
			rtn = left+"."+right;
		}
		
		return rtn;
	}
	
	
	
	
	
}