package com.unomic.dulink.adapter.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.domain.MTConnect;
import com.unomic.dulink.adapter.domain.McPrgmCheckerVo;
import com.unomic.dulink.adapter.service.AdapterService;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.service.DeviceService;
import com.unomic.dulink.sensor.domain.SensorVo;
import com.unomic.dulink.sensor.domain.TemperVo;
import com.unomic.smarti.nfc.service.NFCService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/adapter")
@Controller
public class AdapterController {

	private static final Logger logger = LoggerFactory.getLogger(AdapterController.class);
	
	//private AdapterVo preIOLVo;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	//private HashMap<String, AdapterVo> mapDupleCheck = new HashMap<String, AdapterVo>();

	@Autowired
	private AdapterService adapterService;
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private NFCService nfcService;

	@RequestMapping(value = "getListAllStaff")
	@ResponseBody
    public String getListAllStaff(AdapterVo adapterVo) throws Exception{
        
		List <AdapterVo> listAdapter = adapterService.getListAllStaff();
		//logger.info("run getListAllStaff");
		for(int i=0, size=listAdapter.size();i<size;i++){
			//logger.info(listAdapter.get(i).toString());
		}
        return "OK";
    }
	
	@RequestMapping(value = "test")
	@ResponseBody
    public String testPage(AdapterVo adapterVo) throws Exception{
		
        return "test";
    }
	
	@RequestMapping(value = "pureData")
	@ResponseBody
    public String pureData(@RequestBody String strJson, AdapterVo statusVo, HttpServletRequest request){
		logger.info("run pureData");
		//logger.info("strJson : "+strJson);
		
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject;
        int listSize = 0;
        String adtId = "0";
        Long adtUnixSec = 0L;
		try {
			jsonObject = (JSONObject) jsonParser.parse(strJson);
			JSONArray dataList = (JSONArray)jsonObject.get("data");
			adtUnixSec = Long.parseLong(jsonObject.get("adtTime")+"") * 1000L;
			adtId = jsonObject.get("adtId")+"";
			listSize = dataList.size();

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			return "json ParseException";
		}
		List <AdapterVo> listAdt = json2PureStatus(strJson);
		DeviceVo setVo = new DeviceVo();
		setVo.setAdtId(adtId);
		setVo.setLastUpdateTime(CommonFunction.unixTime2Datetime(adtUnixSec));
		try{
			adapterService.addPureData(setVo, listAdt);
		}catch(Exception e){
			logger.info("AddFail");
			//e.printStackTrace();
			return "add Pure Data FAIL";
		}
		
		return "OK";
	}
	
	// workDate 
	private List<AdapterVo> json2PureStatus(String strJson){
		logger.info("run json2PureStatus");
		List <AdapterVo> listStatusVo = new ArrayList<AdapterVo>();
		try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject;
			jsonObject = (JSONObject) jsonParser.parse(strJson);
			JSONArray dataList = (JSONArray)jsonObject.get("data");
			Long adtUnixSec = Long.parseLong(jsonObject.get("adtTime")+"") * 1000L;
			Long agentUnixSec = System.currentTimeMillis();

			int listSize = dataList.size();
			for(int i = 0 ; i<listSize ; i++){
				AdapterVo pureStatusVo = new AdapterVo();
				JSONObject innerData = (JSONObject) dataList.get(i);
				pureStatusVo.setAdtId(innerData.get("adtId")+"");
				Long statusUnixSec = Long.parseLong(innerData.get("startDateTime")+"") * 1000L;

				//logger.info("adtUnixSec:@@"+adtUnixSec);
				//logger.info("agentUnixSec@:"+agentUnixSec);
				//logger.info("statusUnixSec:"+statusUnixSec);
				pureStatusVo.setStartDateTime(CommonFunction.unixTime2Datetime(statusUnixSec+(agentUnixSec-adtUnixSec)));
				
				pureStatusVo.setMode(innerData.get("mode")+"");
				pureStatusVo.setStatus(innerData.get("status")+"");
				pureStatusVo.setMainPrgmName(getDisplayPrgmName(innerData.get("mainPrgmName")+""));
				pureStatusVo.setCurrentPrgmName(getDisplayPrgmName(innerData.get("currentPrgmName")+""));
				
				
//				String tmpCrtPrgmName = getDisplayPrgmName(innerData.get("currentPrgmName")+"");
//				if(getDisplayPrgmName(innerData.get("currentPrgmName")+"")==null){
//					pureStatusVo.setCurrentPrgmName("INIT");
//				}
//				
	

//				try {
//					pureStatusVo.setPrgmHead(URLEncoder.encode(innerData.get("prgmHead")+"","utf-8"));
//				} catch (UnsupportedEncodingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				pureStatusVo.setPrgmHead(innerData.get("prgmHead")+"");
				pureStatusVo.setModal(innerData.get("modal")+"");
				JSONObject jsonModal = (JSONObject) jsonParser.parse(innerData.get("modal")+"");
				JSONArray listAuxModal = (JSONArray) jsonParser.parse(jsonModal.get("AUX_CODE")+"");
				
				int auxSize = listAuxModal.size();
				
				pureStatusVo.setModal_m1("");
				pureStatusVo.setModal_m2("");
				pureStatusVo.setModal_m3("");
				pureStatusVo.setModal_t("");
				for(int j = 0 ; j<auxSize ; j++){
					JSONObject tmpAux = (JSONObject)listAuxModal.get(j);
					
					if(null != tmpAux.get("M1")){
						pureStatusVo.setModal_m1(tmpAux.get("M1")+"");
					}else if(null != tmpAux.get("M2")){
						pureStatusVo.setModal_m2(tmpAux.get("M2")+"");
					}else if(null != tmpAux.get("M3")){
						pureStatusVo.setModal_m3(tmpAux.get("M3")+"");
					}else if(null != tmpAux.get("T")){
						pureStatusVo.setModal_t(tmpAux.get("T")+"");
					}
				}
				
				JSONArray listGModal = (JSONArray) jsonParser.parse(jsonModal.get("G_MODAL")+"");
				int gModalSize = listGModal.size();
				for(int j = 0 ;j<gModalSize;j++){
					JSONObject tmpGModal = (JSONObject)listGModal.get(j);
					if(null != tmpGModal.get("G0")){
						pureStatusVo.setModal_g0(tmpGModal.get("G0")+"");
					}
				}
				
				pureStatusVo.setSpdLoad(Float.valueOf(innerData.get("spdLoad")+""));
				if (Float.valueOf(innerData.get("spdLoad")+"") == 0 ){
					pureStatusVo.setIsZeroSpdLoad(true);
				}else if(Float.valueOf(innerData.get("spdLoad")+"") > 0){
					pureStatusVo.setIsZeroSpdLoad(false);
				}else{
					logger.error("spdLoad is abnomally.");
					logger.error("spdLoad"+Float.valueOf(innerData.get("spdLoad")+""));
					logger.info("spdLoad"+Float.valueOf(innerData.get("spdLoad")+""));
					pureStatusVo.setIsZeroSpdLoad(null);
				}
				pureStatusVo.setFeedOverride(Float.valueOf(innerData.get("feedOverride")+"").intValue());
			
				if (Integer.valueOf(innerData.get("feedOverride")+"") == 0 ){
					pureStatusVo.setIsZeroFeedOverride(true);
				}else if(Integer.valueOf(innerData.get("feedOverride")+"") > 0){
					pureStatusVo.setIsZeroFeedOverride(false);
				}else{
					logger.error("feedOverride is abnomally.");
					logger.error("feedOverride"+innerData.get("feedOverride")+"");
					pureStatusVo.setIsZeroFeedOverride(null);
				}
				pureStatusVo.setSpdActualSpeed(Float.valueOf(innerData.get("spdActualSpeed")+"").intValue());
				
				if (Integer.valueOf(innerData.get("spdActualSpeed")+"") == 0 ){
					pureStatusVo.setIsZeroActualSpindle(true);
				}else if(Integer.valueOf(innerData.get("spdActualSpeed")+"") > 0){
					pureStatusVo.setIsZeroActualSpindle(false);
				}else{
					logger.error("spdActualSpeed is abnomally.");
					logger.error("spdActualSpeed:"+innerData.get("spdActualSpeed")+"");
					pureStatusVo.setIsZeroActualSpindle(null);
				}
				
				pureStatusVo.setRapidFeedOverride(Float.valueOf(innerData.get("rapidFeedOverride")+"").intValue());
				pureStatusVo.setSpdOverride(Float.valueOf(innerData.get("spdOverride")+"").intValue());
				pureStatusVo.setActualFeed(Float.valueOf(innerData.get("actualFeed")+"").intValue());
				
				pureStatusVo.setRunTimeMin(Integer.valueOf(innerData.get("runTimeMin")+""));
				pureStatusVo.setRunTimeMsec(Integer.valueOf(innerData.get("runTimeMsec")+""));
				pureStatusVo.setAlarm(""+innerData.get("alarm"));
				pureStatusVo.setWorkDate(CommonFunction.mil2WorkDate(statusUnixSec));
				logger.info("result JSON parsing:"+pureStatusVo);
				
				listStatusVo.add(pureStatusVo);
			}
			
        } catch (org.json.simple.parser.ParseException  e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
        	logger.error("JSON parse trash Data");
        	logger.error("strJson:"+strJson);
            return null;
        } catch (NullPointerException e2){
        	logger.error("JSON null trash Data");
        	logger.error("strJson:"+strJson);
        	return null;
        }
		
		return listStatusVo;
	}
	
	
	
	public String makeMD5(String str){
		String MD5 = ""; 
		try{
			MessageDigest md = MessageDigest.getInstance("MD5"); 
			md.update(str.getBytes()); 
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			MD5 = sb.toString();

		}catch(NoSuchAlgorithmException e){
			e.printStackTrace(); 
			MD5 = null; 
		}
		return MD5;
	}
	
	
	@RequestMapping(value="IOLChecker")
	/**
	 * @author cannon
	 * 
	 *
	 * IOL 을 체크하기 위한 실 담당자용 확인 페이지.
	 *  dvc name과 , adt id, ip 를 return 한다.
	 * 
	 * @return test page
	 */
	public String IOLChecker(ModelMap model){
		//adapterService.setIOLData();
		
		List<AdapterVo> listAdt = adapterService.getListIolIp();
		
		model.addAttribute("listAdt", listAdt);
		
		return "test/IOLChecker";
	}
	
	@RequestMapping(value="sendAdtLog")
	@ResponseBody
	public String sendAdtLog(@RequestBody String strJson,AdapterVo getVo, HttpServletRequest req){
		logger.info("run sendAdtLog :["+getVo.getAdtId()+"]");
		//logger.info("testBody:"+strJson);
		//logger.info("req addr:"+req.getRemoteAddr());

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject;
		
		try {
			jsonObject = (JSONObject) jsonParser.parse(strJson);
			
			getVo.setAdtLog(jsonObject.get("STACK_TRACE")+"");
//			logger.info("ANDROID_VERSION:"+jsonObject.get("ANDROID_VERSION")+"");
//			logger.info("STACK_TRACE:"+jsonObject.get("STACK_TRACE")+"");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getVo.setLogTime(CommonFunction.unixTime2Datetime(System.currentTimeMillis()));
		
		//logger.info(getVo.getAdtLog());
		adapterService.addAdtLog(getVo);
		
		return "req addr:"+req.getRemoteAddr();
	}
	
	
	
	@RequestMapping(value="dbTest")
	@ResponseBody
	public String dbTest(AdapterVo getVo, HttpServletRequest req){
		logger.info("run dbTest:"+getVo);
		
		getVo.setLogTime(CommonFunction.unixTime2Datetime(System.currentTimeMillis()));
		
		logger.info(getVo.getAdtLog());
		adapterService.addAdtLog(getVo);
		
		return "req addr:"+req.getRemoteAddr();
	}
	
	@RequestMapping(value="setAdtTime")
	@ResponseBody
	public String setAdtTime(){
		Long sysTime = System.currentTimeMillis() / 1000L;
		logger.info("@@@@@Req setAdtTIme@@@@@");
		
		return sysTime.toString(); 
	}
	
	//no Running.
	@RequestMapping(value="chkAdtHelth")
	@ResponseBody
	public String chkAdtHelth(){
		List<AdapterVo> listRtnVo =  adapterService.getListAdapterHealth();
		
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // the format of your date
		String today = sdf.format(d);
		String installDay = "2015-05-08 17:00:0";
		
		int size=listRtnVo.size();
				
		for(int i=0;i<size;i++){
			AdapterVo tmpVo = listRtnVo.get(i);
			String lastHealthTime = tmpVo.getLastHealthTime();
			logger.info("=====");
			logger.info("adtId:"+tmpVo.getAdtId());
			
			if(null==lastHealthTime){
				tmpVo.setLastHealthTime(installDay);
				tmpVo.setAdtStatus(CommonCode.MSG_ADT_STATUS_NO_REQ);
			}else{
				try {
					Date lastTime = sdf.parse(lastHealthTime);
					Date crntTime = sdf.parse(today);
					Calendar cal = Calendar.getInstance();
					cal.setTime(lastTime);
					Long lastLong = cal.getTimeInMillis();
					cal.setTime(crntTime);
					Long crntLong = cal.getTimeInMillis();
					Long noReqCondition = (long) 300;
					if( noReqCondition > lastLong - crntLong ){
						tmpVo.setAdtStatus(CommonCode.MSG_ADT_STATUS_SEND_REQ);
					}else{
						tmpVo.setAdtStatus(CommonCode.MSG_ADT_STATUS_NO_REQ);
					}
					
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			logger.info("=====");
			listRtnVo.set(i, tmpVo);
		}
		//listRtnVo.
		return "OK"; 
	}
	
	public String getDisplayPrgmName(String name)
	{
		logger.info("name:"+name);
		if(name==null || "null".equals(name)){
			return "";
		}
		String prgName = name;
		
		try{
			prgName = "O" + String.format("%04d", Integer.valueOf(name.substring(1)));
		}catch (Exception e){
			logger.error("Error in PrgmName Convert");
			e.printStackTrace();
		}
		return prgName;
	}
	
	//@RequestMapping(value="getAgentData")
	public void getAgentData(){
		adapterService.setAGTData();

	}
	
	
	public AdapterVo getAGTStatus(MTConnect mtc, String adtId ){
		
		JSONParser jsonParser = new JSONParser();
		
		AdapterVo pureStatusVo = new AdapterVo();		
		pureStatusVo.setAdtId(adtId);
		Long statusUnixSec = CommonFunction.mtcDateTime2Mil(mtc.getCreationTime());
		pureStatusVo.setStartDateTime(CommonFunction.unixTime2Datetime(statusUnixSec));
		
		pureStatusVo.setStatus(mtc.getNc_status());
		pureStatusVo.setMainPrgmName(mtc.getProgram());
		pureStatusVo.setPrgmHead(mtc.getProgram_header());
		
		pureStatusVo.setAlarm(genJsonAlarm(mtc.getAlarm1(),mtc.getAlarm2()));
		pureStatusVo.setModal(genJsonModal(mtc.getModal()));

		try {
			JSONObject jsonModal;
			JSONArray listAuxModal;
			
			jsonModal = (JSONObject) jsonParser.parse(genJsonModal(mtc.getModal()));
			listAuxModal = (JSONArray) jsonParser.parse(jsonModal.get("AUX_CODE")+"");
			int auxSize = listAuxModal.size();
			
			pureStatusVo.setModal_m1("");
			pureStatusVo.setModal_m2("");
			pureStatusVo.setModal_m3("");
			pureStatusVo.setModal_t("");
			for(int j = 0 ; j<auxSize ; j++){
				JSONObject tmpAux = (JSONObject)listAuxModal.get(j);
				
				if(null != tmpAux.get("M1")){
					pureStatusVo.setModal_m1(tmpAux.get("M1")+"");
				}else if(null != tmpAux.get("M2")){
					pureStatusVo.setModal_m2(tmpAux.get("M2")+"");
				}else if(null != tmpAux.get("M3")){
					pureStatusVo.setModal_m3(tmpAux.get("M3")+"");
				}else if(null != tmpAux.get("T")){
					pureStatusVo.setModal_t(tmpAux.get("T")+"");
				}
			}
			
			JSONArray listGModal = (JSONArray) jsonParser.parse(jsonModal.get("G_MODAL")+"");
			int gModalSize = listGModal.size();
			for(int j = 0 ;j<gModalSize;j++){
				JSONObject tmpGModal = (JSONObject)listGModal.get(j);
				if(null != tmpGModal.get("G0")){
					pureStatusVo.setModal_g0(tmpGModal.get("G0")+"");
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pureStatusVo.setSpdLoad(Float.parseFloat(mtc.getSpindle_load()));
		if (Float.valueOf(mtc.getSpindle_load()) == 0 ){
			pureStatusVo.setIsZeroSpdLoad(true);
		}else if(Float.valueOf(mtc.getSpindle_load()) > 0){
			pureStatusVo.setIsZeroSpdLoad(false);
		}else{
			logger.error("spdLoad is abnomally.");
			logger.info("spdLoad"+mtc.getSpindle_load());
			pureStatusVo.setIsZeroSpdLoad(null);
		}
		pureStatusVo.setFeedOverride(Float.valueOf(mtc.getFeed_override()).intValue());
		pureStatusVo.setRapidFeedOverride(Float.valueOf(mtc.getRapid_override()).intValue());
		pureStatusVo.setSpdOverride(Float.valueOf(mtc.getSpindle_override()).intValue());
		pureStatusVo.setActualFeed(Float.valueOf(mtc.getActual_feed()).intValue());
		pureStatusVo.setSpdActualSpeed(Float.valueOf(mtc.getSpindle_actual_speed()).intValue());
		pureStatusVo.setMode(mtc.getMode());
		pureStatusVo.setRunTimeMin(0);
		pureStatusVo.setRunTimeMsec(0);

		pureStatusVo.setWorkDate(CommonFunction.mil2WorkDate(statusUnixSec));
	    
		return pureStatusVo;
	}
	
	private String genJsonAlarm(String strAlarm1, String strAlarm2){
	//private String genJsonAlarm(String alarmCode1, String alarmMsg1, String alarmCode2,String alarmMsg2){
		//[{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"},{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"}]
		
		JSONArray innerArray = new JSONArray();
		JSONObject innerObject1 = new JSONObject();
		JSONObject innerObject2 = new JSONObject();
		
		String[] arrAlarm1;
		String[] arrAlarm2;
		String alarmMsg1="";
		String alarmCode1="";
		String alarmMsg2="";
		String alarmCode2="";
		if(!strAlarm1.isEmpty()){
			arrAlarm1 = strAlarm1.split(" ");
			alarmCode1 = arrAlarm1[0];
			alarmMsg1 = arrAlarm1[1];
		}
		if(!strAlarm2.isEmpty()){
			arrAlarm2 = strAlarm2.split(" ");
			alarmCode2 = arrAlarm2[0];
			alarmMsg2 = arrAlarm2[1];
		}

		innerObject1.put("alarmMsg", alarmMsg1);
		innerObject1.put("alarmCode", alarmCode1);
		innerObject2.put("alarmMsg", alarmMsg2);
		innerObject2.put("alarmCode", alarmCode2);
		innerArray.add(innerObject1);
		innerArray.add(innerObject2);
		
		return innerArray.toString();
	}
	
	private String genJsonModal(String modal){
		String[] arrModal = modal.split(" ");
		List<String> arrM = new ArrayList<String>();
		int size = arrModal.length;
		
		String gCode = "";
		String tCode = "";
		for(int i = 0 ; i<size ; i++){
			
			if(arrModal[i].charAt(0)=='G'){
				gCode = arrModal[i].substring(1);
			}else if(arrModal[i].charAt(0)=='T'){
				tCode = arrModal[i].substring(1);
			}else if(arrModal[i].charAt(0)=='M'){
				arrM.add(arrModal[i].substring(1));
			}
		}

		JSONObject rootObj = new JSONObject();
		JSONArray arrGmodal = new JSONArray();
		JSONObject objG0 = new JSONObject();
		JSONArray arrAuxCode= new JSONArray();
		JSONObject objM1 = new JSONObject();
		JSONObject objM2 = new JSONObject();
		JSONObject objM3 = new JSONObject();
		JSONObject objT = new JSONObject();
		
		objG0.put("G0", gCode); arrGmodal.add(objG0);
		objT.put("T", tCode); arrAuxCode.add(objT);
		
		for(int i = 0 ; i < arrM.size() ; i++){
			if(i==0){
				objM1.put("M1", arrM.get(i)); arrAuxCode.add(objM1);
			}else if(i==1){
				objM2.put("M2", arrM.get(i)); arrAuxCode.add(objM2);
			}else if(i==2){
				objM3.put("M2", arrM.get(i)); arrAuxCode.add(objM3);
			}
		}

		rootObj.put("G_MODAL", arrGmodal);
		rootObj.put("AUX_CODE", arrAuxCode);
		
		return rootObj.toString();
	}
	
	@RequestMapping(value="chkAdt")
	@ResponseBody
	public String chdDvc(AdapterVo adtVo){
		AdapterVo getAdt = adapterService.chkAdtStatus(adtVo);
		
		JSONObject rootJson = new JSONObject();
		
		rootJson.put("ADT_ID", adtVo.getAdtId());
		rootJson.put("regdt", adtVo.getRegdt());
		rootJson.put("start_date_time", adtVo.getStartDateTime());
		rootJson.put("end_date_time", adtVo.getEndDateTime());
		rootJson.put("mode", adtVo.getMode());
		rootJson.put("status", adtVo.getStatus());
		rootJson.put("chart_status", adtVo.getChartStatus());
		rootJson.put("main_prgm_name", adtVo.getMainPrgmName());
		rootJson.put("spd_load", adtVo.getSpdLoad());
		rootJson.put("feed_override", adtVo.getFeedOverride());
		rootJson.put("rapid_feed_override", adtVo.getRapidFeedOverride());
		rootJson.put("alarm", adtVo.getAlarm());
		rootJson.put("modal", adtVo.getModal());
		rootJson.put("prgm_head", adtVo.getPrgmHead());
		
		return rootJson.toString(); 
	}
	
	@RequestMapping(value="testDateStarter")
	@ResponseBody
	public String testDateStarter(String startTime, String endTime){
		if(CommonFunction.isDateStart(startTime, endTime)){
			return "true";
		}else{
			return "false";
		}
	}
	
	@RequestMapping(value="testStartTime")
	@ResponseBody
	public String testStartTime(String crtTime){

		System.out.println("crtTime:" + crtTime);
		//CommonFunction.unixTime2Datetime(System.currentTimeMillis());
		
		String stTime = CommonFunction.getChartStartTime(crtTime);
		
		return stTime;
	}
	
	@RequestMapping(value="getAdtData")
	@ResponseBody
	public List<AdapterVo> getAdtData(String adtId,String date) {
		logger.info("getDbData");
		logger.info("adtId:"+adtId);
		logger.info("date:"+date);
		
		Date today = new Date();
		SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cDateTime = sdfTime.format(today);
		
		String result = "";
		String[] chartFormat;
		AdapterVo inputVo = new AdapterVo();
		inputVo.setAdtId(adtId);
		inputVo.setWorkDate(date);
		List<AdapterVo> listStatus= nfcService.getListStatusDate(inputVo);

		
		return listStatus;
	}
	
	/*
	 * sDate=yyyy-MM-dd
	 * eDate=yyyy-MM-dd
	 */

	@RequestMapping(value="getApcList")
	public String getApcList(McPrgmCheckerVo inputVo, ModelMap model){
		try {
			List<McPrgmCheckerVo> rtnList = adapterService.getListAPCDvc(inputVo);
			model.put("listApc", rtnList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return "apc/listApc";
	};
	
	@RequestMapping(value="getApcListCSV")
	@ResponseBody
	public List<McPrgmCheckerVo> getApcListCvs(McPrgmCheckerVo inputVo) {

		List<McPrgmCheckerVo> listStatus= adapterService.getListAPCDvc(inputVo);

		
		return listStatus;
	}

	@RequestMapping(value = "csv")
	public String csv(String csv) {
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		return "csv/csv1"; 
	}
	
	
	@RequestMapping(value="apc")
	public String sensorPage(){
		
		return "apc/ApcInfo";
	}
	@RequestMapping(value="editLastData")
	@ResponseBody
	public String editLastData(AdapterVo inputVo){
		adapterService.editLastStatus(inputVo);
		
		return "OK";
	}
	

};

