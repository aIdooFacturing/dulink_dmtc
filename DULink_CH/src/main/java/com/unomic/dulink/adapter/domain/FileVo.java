package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
public class FileVo{
	private String name, pwd, title, content, fileName;
    private MultipartFile uploadfile;
}
