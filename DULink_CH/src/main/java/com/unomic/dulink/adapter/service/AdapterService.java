package com.unomic.dulink.adapter.service;

import java.util.List;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.domain.McPrgmCheckerVo;
import com.unomic.dulink.adapter.domain.McPrgmVo;
import com.unomic.dulink.device.domain.DeviceVo;

public interface AdapterService {
	
	public List<AdapterVo> getListAllStaff();
	public List<McPrgmVo> getListMcPrgm();
	public String addPureStatus(AdapterVo pureStatusVo);
	public String addListPureStatus(List<AdapterVo> listPureStatus);
	public String editLastEndTime(AdapterVo firstOfListVo);
	public String addIOLStatus(AdapterVo pureStatusVo);
	public String addAdtLog(AdapterVo adtVo);
	public List<AdapterVo> getListIOL();
	public String editAdapterHealth(AdapterVo adapterVo);
	public List<AdapterVo> getListAdapterHealth();
	public List<AdapterVo> getListWinAgent();
	public AdapterVo chkAdtStatus(AdapterVo adtVo);
	public int cntAdtStatus(AdapterVo adtVo);
	public String addPureData(DeviceVo dvcVo, List<AdapterVo> listAdt);
	public void getIOLData();
	public void setAGTData();
	public List<AdapterVo> getListIolIp();
	public List<McPrgmCheckerVo> getListAPCDvc(McPrgmCheckerVo inputVo);
	public String editLastStatus(AdapterVo pureStatusVo);
};
