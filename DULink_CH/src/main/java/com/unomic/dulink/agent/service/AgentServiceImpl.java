package com.unomic.dulink.agent.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.domain.AdapterVo;


@Service
@Repository
public class AgentServiceImpl extends SqlSessionDaoSupport implements AgentService{
	private final static String ADAPTER_SPACE= "com.dulink.adapter.";
	
	@Override
	public List<AdapterVo> getListAllStaff(){
		// TODO Auto-generated method stub
		SqlSession sqlSession=getSqlSession();
		List<AdapterVo> rtnList = sqlSession.selectList(ADAPTER_SPACE+"getListAllAdapter");
		return rtnList;
	}
	
	@Override
	@Transactional
	public String addPureStatus(AdapterVo pureStatusVo)
	{
		SqlSession sqlSession=getSqlSession();
		logger.info("pureStatusVo:"+pureStatusVo);
		sqlSession.insert(ADAPTER_SPACE+"addPureData", pureStatusVo);
		return "OK";
	}
	
	@Override
	@Transactional
	public String addIOLStatus(AdapterVo pureStatusVo)
	{
		SqlSession sqlSession=getSqlSession();
		logger.info("pureStatusVo:"+pureStatusVo);
		sqlSession.insert(ADAPTER_SPACE+"addIOLData", pureStatusVo);
		return "OK";
	}
	
	@Override
	@Transactional
	public String addListPureStatus(List<AdapterVo> listPureStatus)
	{
		SqlSession sqlSession=getSqlSession();
		
		
		AdapterVo firstAdapterVo = listPureStatus.get(0);
		AdapterVo tmpAdapterVo = (AdapterVo) sqlSession.selectOne(ADAPTER_SPACE + "getLastStartTime", firstAdapterVo);
		firstAdapterVo.setEndDateTime(tmpAdapterVo.getStartDateTime());
		
		
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("listPureStatus", listPureStatus);
		
		sqlSession.insert(ADAPTER_SPACE + "addListPureData", dataMap);

		return "OK";
	}

	@Override
	@Transactional
	public String editLastEndTime(AdapterVo firstOfListVo)
	{
		SqlSession sql=getSqlSession();
		int cnt = (Integer) sql.selectOne(ADAPTER_SPACE + "cntAdapterStatus", firstOfListVo);
		if(cnt>0){
			AdapterVo tmpAdapterVo = (AdapterVo) sql.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			//firstOfListVo.setEndDateTime(tmpAdapterVo.getStartDateTime());
			tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
			sql.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
		}

		return "OK";
	}
	
}