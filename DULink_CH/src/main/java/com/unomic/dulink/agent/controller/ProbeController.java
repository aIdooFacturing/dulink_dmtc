package com.unomic.dulink.agent.controller;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.mtconnect.mtconnectdevices.CategoryType;
import org.mtconnect.mtconnectdevices.ComponentsType;
import org.mtconnect.mtconnectdevices.ControllerType;
import org.mtconnect.mtconnectdevices.DataItemType;
import org.mtconnect.mtconnectdevices.DataItemsType;
import org.mtconnect.mtconnectdevices.DeviceType;
import org.mtconnect.mtconnectdevices.DevicesType;
import org.mtconnect.mtconnectdevices.ElectricType;
import org.mtconnect.mtconnectdevices.HeaderType;
import org.mtconnect.mtconnectdevices.MTConnectDevicesType;
import org.mtconnect.mtconnectdevices.PathType;
import org.mtconnect.mtconnectdevices.SourceType;
import org.mtconnect.mtconnectdevices.SystemsType;
import org.mtconnect.mtconnectstreams.BlockType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.service.AdapterService;
import com.unomic.dulink.agent.domain.AgentVo;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value="/")
@Controller
public class ProbeController {

	private static final Logger logger = LoggerFactory.getLogger(ProbeController.class);
	private static final String FILE_NAME = "mtc_probe.xml";
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	private HashMap<String, AdapterVo> mapDupleCheck = new HashMap<String, AdapterVo>();
	
	@Autowired
	private AdapterService adapterService;

	@RequestMapping(value = "/{dvcId}/probe", method = RequestMethod.GET)
	@ResponseBody
    public String getDvcProbe(@PathVariable String dvcId, AgentVo agentVo) throws Exception{
		
		logger.info("probe_dvcId:"+dvcId);
		//logger.info(agentVo.getDvcId());
        return "OK";
    }
	
	@RequestMapping(value = "/probe", method = RequestMethod.GET)
	@ResponseBody
    public String getProbe(AgentVo agentVo) throws Exception{
		
		logger.info("just probe");
        return "OK";
    }
	
	public void xmlTest() throws JAXBException
    {
				  
//		MTConnectDevicesType mtcDevices = new MTConnectDevicesType();
//		ComponentsType components = new ComponentsType();
//		
//        JAXBContext context = JAXBContext.newInstance("generated");
//        JAXBElement<MTConnectDevicesType> element = mtcDevices.createExpenseReport(expense);
//        
//        Marshaller marshaller = context.createMarshaller();
//        marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
//        marshaller.marshal(element,System.out);
    }
	
	@RequestMapping(value="probe2")
	@ResponseBody
	public MTConnectDevicesType probe(){

		
		DataItemsType dataItems = new DataItemsType();
		
		
		DataItemType mode = new DataItemType();
		mode.setCategory(CategoryType.EVENT);
		mode.setId("mode");
		mode.setType("CONTROLLER_MODE");
		dataItems.getDataItem().add(mode);
		
		DataItemType ncStatus = new DataItemType();
		ncStatus.setCategory(CategoryType.EVENT);
		ncStatus.setId("nc_status");
		ncStatus.setType("EXECUTION");
		dataItems.getDataItem().add(ncStatus);
		
		DataItemType spd_override = new DataItemType();
		spd_override.setCategory(CategoryType.EVENT);
		spd_override.setId("spindle_override");
		spd_override.setType("OVERRIDE");
		spd_override.setNativeUnits("PERCENT");
		dataItems.getDataItem().add(spd_override);
		
		DataItemType spd_load = new DataItemType();
		spd_load.setCategory(CategoryType.SAMPLE);
		spd_load.setId("spindle_load");
		spd_load.setType("LOAD");
		spd_load.setNativeUnits("PERCENT");
		dataItems.getDataItem().add(spd_load);
		
		DataItemType spd_actual_speed = new DataItemType();
		spd_actual_speed.setCategory(CategoryType.SAMPLE);
		spd_actual_speed.setId("spindle_actual_speed");
		spd_actual_speed.setType("OVERRIDE");
		spd_actual_speed.setNativeUnits("PERCENT");
		dataItems.getDataItem().add(spd_actual_speed);
		
		DataItemType feedOverride = new DataItemType();
		feedOverride.setCategory(CategoryType.EVENT);
		feedOverride.setId("feed_override");
		feedOverride.setType("AXIS_FEEDRATE_OVERRIDE");
		feedOverride.setSubType("PROGRAMMED");
		dataItems.getDataItem().add(feedOverride);
		
		DataItemType feedRate = new DataItemType();
		feedRate.setCategory(CategoryType.EVENT);
		feedRate.setId("feed_rate");
		feedRate.setType("AXIS_FEEDRATE_OVERRIDE");
		feedRate.setSubType("PROGRAMMED");
		dataItems.getDataItem().add(feedOverride);
		
		DataItemType rapidOverride = new DataItemType();
		rapidOverride.setCategory(CategoryType.EVENT);
		rapidOverride.setId("rapid_override");
		rapidOverride.setType("AXIS_FEEDRATE_OVERRIDE");
		rapidOverride.setSubType("RAPID");
		dataItems.getDataItem().add(rapidOverride);
		
		DataItemType actualFeed = new DataItemType();
		actualFeed.setCategory(CategoryType.SAMPLE);
		actualFeed.setId("actual_feed");
		actualFeed.setType("ACTUAL_FEED");
		actualFeed.setSubType("FEED");
		dataItems.getDataItem().add(actualFeed);
		
		DataItemType runTime = new DataItemType();
		runTime.setCategory(CategoryType.SAMPLE);
		runTime.setId("run_time");
		runTime.setType("ACCUMULATED_TIME");
		runTime.setUnits("SECOND");
		dataItems.getDataItem().add(runTime);
		
		PathType path = new PathType();
		
		path.setDataItems(dataItems);
		ComponentsType components = new ComponentsType();
		
		org.mtconnect.mtconnectdevices.ObjectFactory factory = new org.mtconnect.mtconnectdevices.ObjectFactory(); 
		JAXBElement<PathType> elemPath = factory.createPath(path);
		
		components.getComponent().add(elemPath);
		
		ControllerType controller = new ControllerType();
		controller.setId("cont");
		controller.setName("controller");
		DataItemsType contDataItems = new DataItemsType();
		
		DataItemType program = new DataItemType();
		program.setCategory(CategoryType.EVENT);
		program.setId("program");
		program.setType("PROGRAM");
		contDataItems.getDataItem().add(program);
		
		DataItemType programHeader = new DataItemType();
		programHeader.setCategory(CategoryType.EVENT);
		programHeader.setId("program_header");
		programHeader.setType("PROGRAM_HEADER");
		contDataItems.getDataItem().add(programHeader);
		
		DataItemType block = new DataItemType();
		block.setCategory(CategoryType.EVENT);
		block.setId("modal");
		block.setType("BLOCK");
		contDataItems.getDataItem().add(block);
		
		DataItemType alarm1 = new DataItemType();
		alarm1.setCategory(CategoryType.CONDITION);
		alarm1.setId("alarm1");
		alarm1.setType("ACTUATOR");
		contDataItems.getDataItem().add(alarm1);
		
		DataItemType alarm2 = new DataItemType();
		alarm2.setCategory(CategoryType.CONDITION);
		alarm2.setId("alarm2");
		alarm2.setType("ACTUATOR");
		contDataItems.getDataItem().add(alarm2);
		
		
		controller.setDataItems(contDataItems);
		controller.setComponents(components);
		
		ComponentsType dvcComponents = new ComponentsType();
		JAXBElement<ControllerType> elemController = factory.createController(controller);
		dvcComponents.getComponent().add(elemController);
		
		DeviceType device = new DeviceType();
		device.setId("1006964");
		device.setName("D2780");
		device.setUuid("1006964");
		device.setComponents(dvcComponents);
		
		DevicesType devices = new DevicesType();
		devices.getDevice().add(device);

		HeaderType header = new HeaderType();
		//header.setCreationTime("2014-11-12T05:36:51Z");
		GregorianCalendar c = new GregorianCalendar();
		
		//c.setTime("2014-11-12T05:36:51Z");
		//XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		XMLGregorianCalendar cal = null;
		try {
			cal = DatatypeFactory.newInstance().newXMLGregorianCalendar("2015-04-23T10:36:51Z");
		} catch (DatatypeConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		header.setCreationTime(cal);
		header.setSender("unomic-Agent");
		BigInteger bigInstanceId = new BigInteger("1415770578");
		header.setInstanceId(bigInstanceId);
		header.setVersion("1.3.0.7");
		header.setBufferSize(131072);
		//ArrayList<HeaderType> headerList = new ArrayList<HeaderType>();
		//headerList.add(header);

		//MTConnectStreams mtConStrm = new MTConnectStreams();
		MTConnectDevicesType mtConProbe = new MTConnectDevicesType();
		
		mtConProbe.setHeader(header);
		mtConProbe.setDevices(devices);
		
		

		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(MTConnectDevicesType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "urn:mtconnect.org:MTConnectDevices:1.3 /schemas/MTConnectDevices_1.3.xsd");
			//jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml-stylesheet type='text/xsl' href='foobar.xsl' ?>"); 
			File XMLfile = new File(FILE_NAME);


			try{
				jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", "<?xml-stylesheet type='text/xsl' href='foobar.xsl' ?>");
			}
			catch(PropertyException pex)
			{
				jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml-stylesheet type='text/xsl' href='http://106.240.234.114:8080/media/Streams.xsl' ?>");
			}

			jaxbMarshaller.marshal(mtConProbe, XMLfile);
			jaxbMarshaller.marshal(mtConProbe, System.out);
		}catch (Exception e) {
			e.printStackTrace();
		};
		return mtConProbe;
	}
	
//	private static void jaxbObjectToXML(Employee emp) {
//		 
//        try {
//            JAXBContext context = JAXBContext.newInstance(Employee.class);
//            Marshaller m = context.createMarshaller();
//            //for pretty-print XML in JAXB
//            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
// 
//            // Write to System.out for debugging
//            // m.marshal(emp, System.out);
// 
//            // Write to File
//            m.marshal(emp, new File(FILE_NAME));
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//    }
};

