package com.unomic.dulink.staff.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.staff.domain.StaffVo;

@Service
@Repository
public class StaffServiceImpl extends SqlSessionDaoSupport implements StaffService{

	private final static String namespace= "com.DULink.staff.";
	private final static String shopspace= "com.DULink.shop.";
	
	@Override
	@Transactional
	public Map setStaffRegId(StaffVo staffVo) throws Exception {
		// TODO Auto-generated method stub
		SqlSession sqlSession=getSqlSession();
		String result="";
		Map map = new HashMap();
		int cnt = (Integer) sqlSession.selectOne(namespace+"countStaffPhone", staffVo);
		if(0==cnt){
			result="no";
		}else if (1==cnt){
			StaffVo getStaff = (StaffVo) sqlSession.selectOne(namespace+"getStaffbyPhone", staffVo);
			staffVo.setStaffId(getStaff.getStaffId());
			sqlSession.update(namespace+"setUserRegId", staffVo);
			map.put("staffId", getStaff.getStaffId());
			map.put("staffName", getStaff.getStaffName());
			map.put("teamId", getStaff.getTeamId());
			map.put("teamName", getStaff.getTeamName());
			map.put("position", getStaff.getPosition());
			map.put("cellNum", getStaff.getCellNum());
			result="ok";
		}else{
			result="duple";
		}
		map.put("result", result);
		
		return map;
	}
	
	
	public List<StaffVo> getListStaff(StaffVo staffVo) throws Exception {
		// TODO Auto-generated method stub
		SqlSession sqlSession=getSqlSession();
		
		List<StaffVo> rtnList = sqlSession.selectList(namespace+"getListStaff", staffVo);
		
		return rtnList;
		
	}
	@Override
	public StaffVo getStaff(StaffVo staffVo) throws Exception {
		// TODO Auto-generated method stub
		SqlSession sqlSession=getSqlSession();
		
		StaffVo getVo= (StaffVo) sqlSession.selectOne(namespace+"getStaff",staffVo);
		
		return getVo;
	}

}
