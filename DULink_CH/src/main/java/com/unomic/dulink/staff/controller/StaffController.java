package com.unomic.dulink.staff.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.staff.domain.StaffVo;
import com.unomic.dulink.staff.service.StaffService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/staff")
@Controller
public class StaffController {
	
	private static final Logger logger = LoggerFactory.getLogger(StaffController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private StaffService staffService;
	
	
	@RequestMapping(value ="setStaffRegId")
	public void setStaffRegId(StaffVo staffVo, HttpServletResponse response)throws Exception{
		logger.info("run setStaffRegId");
		response.setContentType("text/html;charset=utf-8"); //한글깨짐방지
		PrintWriter writer=response.getWriter();
		String str="";
		String json="";
		ObjectMapper om = new ObjectMapper();
		Map map = new HashMap();
		try {
			map = staffService.setStaffRegId(staffVo);
			//str = staffService.setStaffRegId(staffVo);
			//map.put("result",str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			str = "error";
			map.put("result",str);
		}
			json=om.defaultPrettyPrintingWriter().writeValueAsString(map);
			writer.write(json);
			writer.flush();
			writer.close();
	}
	
	@RequestMapping(value ="getListStaff")
	
	public void getListStaff(HttpServletResponse response,StaffVo staffVo)throws Exception{
		logger.info("run setUserRegId");
		response.setContentType("text/html;charset=utf-8"); //한글깨짐방지
		PrintWriter writer=response.getWriter();
		String str="";
		ObjectMapper om = new ObjectMapper();
		Map map = new HashMap();
		try {
			List<StaffVo> listStaff = staffService.getListStaff(staffVo);
			map.put("listStaff",listStaff);
			str=om.defaultPrettyPrintingWriter().writeValueAsString(map);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			str = "error";
			map.put("result",str);
		}
			writer.write(str);
			writer.flush();
			writer.close();
	}
}
