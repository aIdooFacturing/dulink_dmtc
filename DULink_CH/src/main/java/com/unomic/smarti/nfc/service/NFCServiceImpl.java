package com.unomic.smarti.nfc.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.cnc.data.MemoryMap;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.smarti.nfc.domain.NFCVo;


@Service
@Repository
public class NFCServiceImpl extends SqlSessionDaoSupport implements NFCService{

	private final static String ADAPTER_SPACE= "com.dulink.adapter.";
	private boolean first = true;
	private ArrayList<Double> arrStatusTime = new ArrayList<Double>();
	private ArrayList<String> arrStatusColor = new ArrayList<String>();
	private String pMinute = "";
	private int idx = 0;
	private String pStatus = "";
	private int accCnt = 1;
	private String today = "empty";
	private String timeStamp = "empty";
	private String cTime = "Adapter disconnect";
	
	private HashMap<String,AdapterVo> mapDupleChecker = new HashMap<String, AdapterVo>();
	
	@Override
	public String getToday(){
		return this.today;
	}
	@Override
	public void setToday(String today){
		this.today = today;
	}
	
	@Override
	public String getTimeStamp(){
		return this.timeStamp;
	}
	
	@Override
	public void setCTime(String cTime){
		this.cTime = cTime;
	}
	@Override
	public String getCTime(){
		return this.cTime;
	}
	
	@Override
	public void setTimeStamp(String timeStamp){
		this.timeStamp = timeStamp;
	}
	
	@Override
	public void insertData(MemoryMap Map) {
		SqlSession sql = getSqlSession();
	}

	@Override
	public void writeStatus(NFCVo nfcVo) {
		String ncStatus = null;
		ncStatus = nfcVo.getNcStatus();

		long unixSeconds = nfcVo.getTimeStamp();
		Date date = new Date(unixSeconds*1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
		
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
		this.cTime = sdfTime.format(date);

		SimpleDateFormat sdfToday = new SimpleDateFormat("yyyyMMdd");
		String localToday = sdfToday.format(date);
		//현재 시간(10분 단위)
		SimpleDateFormat sdf2 = new SimpleDateFormat("HHmm");
		String minute = sdf2.format(date);
		String hour = minute.substring(0,2);
		minute = minute.substring(2,3);

		//status Color set
		String statusColor = "";
		String status = nfcVo.getNcStatus();
		String alarm;
		try {
			alarm = URLEncoder.encode(nfcVo.getAlarmArray(),"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			return;
		}
		
		if(status.equals("STRT") || status.equals("START")){
			statusColor = "green";
		}else if((!status.equals("STRT") || !status.equals("START")) && alarm.equals("")){
			statusColor = "yellow";
		}else if(!alarm.equals("")){
			statusColor = "red";
		};

		//차트 한 칸 크기 
		double time = (Double.parseDouble(hour) + 0.16 * Double.parseDouble(minute));

		//첫 번째 칸 채움 (0 ~ 시작 점)
		if(this.first){
			if(time>0){
				time -= 0.16;
			};
			this.arrStatusTime.add(time);
			this.arrStatusColor.add("white");
			this.first = false;
		};

		//10분 단위 변경 시 데이터 추가
		if(!pMinute.equals(minute)){
			this.arrStatusTime.add(0.16);
			this.arrStatusColor.add(statusColor);	
			this.pMinute = minute;
			this.idx++;
		};

		//알람 or 초록불에서 노랑불이면 arrayData 변경
		if(statusColor.equals("red") || 
				( pStatus.equals("green") && statusColor.equals("yellow") && !this.arrStatusColor.get(idx).equals("red"))){
			this.arrStatusColor.set(this.idx,statusColor);
			this.pMinute=minute;
		};

		this.pStatus = statusColor;

		BufferedWriter bwStatusFile;
		File statusFile;
		
		//Add file check Routine.
		//empty일 경우 새로 만들기.
		if(this.today.equals("empty")){
			this.today = localToday;
			statusFile = new File(this.today + CommonCode.FILE_EXTEND);
			if(statusFile.exists()){
				chkInvalidate(this.today);
			}
		//오늘 날짜와 다를 경우 파일 내용 초기화.
		}else if(!this.today.equals(localToday)){
			this.today = localToday;
			initStatusData();
      	}
		//그외 경우 그냥 값 추가해서 쓰기.
      	//add Cannon
		
		try {
			bwStatusFile = new BufferedWriter(new FileWriter(this.today + CommonCode.FILE_EXTEND));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			logger.error(e1.toString());
			return;
		}

		String statusChartData = "";
		String contents = "";
		String statusTimeTxt = "";
		String statusColorTxt = "";
		String comma = "";
		String comma2 = "";
		for(int i = 0; i < this.arrStatusTime.size(); i++){
			statusTimeTxt += comma + arrStatusTime.get(i);
			comma = ",";
		};

		for(int i = 0; i < this.arrStatusColor.size(); i++){
			statusColorTxt += comma2 + arrStatusColor.get(i);
			comma2 = ",";
		};

		statusChartData = statusTimeTxt + "/" + statusColorTxt;

		try {
			contents += statusChartData+ "/" +
					nfcVo.getSpindleLoad() + "/" + 
					nfcVo.getFeedOverride() + "/" + 
					nfcVo.getMainProgName() + "/" + 
					nfcVo.getAlarmNumber() + "/" + 
					ncStatus + "/" + 
					URLEncoder.encode(nfcVo.getAlarmArray(),"utf-8") + "/" + 
					nfcVo.getArrAlarm() + "/" +
					this.accCnt;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
		}
		accCnt++;

		try {
			bwStatusFile.write(contents);
			bwStatusFile.flush();
			bwStatusFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		}
		if(CommonCode.TARGET.equals("client")){
			sendGlobal(nfcVo);
		}
	}
	
	public void sendGlobal(NFCVo nfcVo) {
		logger.info("run sendGlobal");
		try{
			URL url = new URL("http://" + CommonCode.SERVER_IP + CommonCode.POST_URL);
			Map<String,Object> params = new LinkedHashMap();
			String localhost = InetAddress.getLocalHost().toString();
			StringTokenizer st = new StringTokenizer(localhost, "/");
			String host = st.nextToken();

			params.put("timeStamp", nfcVo.getTimeStamp());
			params.put("mainProgName", nfcVo.getMainProgName());
			params.put("ncStatus", nfcVo.getNcStatus());
			params.put("spindleLoad", nfcVo.getSpindleLoad());
			params.put("feedOverride", nfcVo.getFeedOverride());
			params.put("alarmType1", nfcVo.getAlarmType1());
			params.put("alarmNumber1", nfcVo.getAlarmNumber1());
			params.put("alarmMsg1", nfcVo.getAlarmMsg1());
			params.put("alarmType2", nfcVo.getAlarmType2());
			params.put("alarmNumber2", nfcVo.getAlarmNumber2());
			params.put("alarmMsg2", nfcVo.getAlarmMsg2());
			params.put("MD5", nfcVo.getMD5());

			StringBuilder postData = new StringBuilder();
			for (Map.Entry<String,Object> param : params.entrySet()) {
				if (postData.length() != 0) postData.append('&');
				postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			}
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(postDataBytes);

			Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			String tmpLog="";
			
			//for (int c; (c = in.read()) >= 0; System.out.print((char)c));

			for (int c; (c = in.read()) >= 0; tmpLog += ((char)c) );
			logger.info("ContentsOfBuffer:"+tmpLog);
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("fail sendData");
			logger.error(e.toString());
		};
	}
	
	public void chkInvalidate(String txt){
		String result = "";
		File aFile = new File(txt + ".txt");
		FileReader fileReader;
		try {
			fileReader = new FileReader(aFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			return;
		}
		BufferedReader reader = new BufferedReader(fileReader);
		String line = null;
		
		try {
			while((line = reader.readLine())!=null){
				result += line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.toString());
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.toString());
		}
		
		String[] index = result.split("/");
		String timeArray = index[0];
		String colorArray = index[1];
		
		String[] timeIdx = timeArray.split(",");
		String[] colorIdx = colorArray.split(",");
		
		this.idx = timeIdx.length;
		
		//txt 파일에 저장 된 시간 합
		double recodedTime = 0.00;
		
		for(int i = 0; i < timeIdx.length; i++){
			recodedTime += Double.parseDouble(timeIdx[i]);
		};
		
		//현재 시간과 비교
		Date d = new Date();
	     
		SimpleDateFormat sdf = new SimpleDateFormat("HH");
		SimpleDateFormat sdf2 = new SimpleDateFormat("mm");
		int hour = Integer.parseInt(sdf.format(d));
		double minute = Double.parseDouble(sdf2.format(d).substring(0,1));
		double currentTime = hour + 0.16*minute; 

		//기록되지 않은 값
		DecimalFormat df=new DecimalFormat("###,##0.00");
		String lossTime = df.format(currentTime - recodedTime);		

		logger.info("currentTime:"+currentTime);
		logger.info("recodedTime:"+recodedTime);
		logger.info("lossTime:"+lossTime);
		
		//현재 시간보다 기록시간이 작으면
		if(recodedTime<currentTime){
			//배열 초기화 후 값 삽입
			arrStatusTime = new ArrayList<Double>();
			arrStatusColor = new ArrayList<String>();
			
			for(int i = 0; i < timeIdx.length; i++){
				arrStatusTime.add(Double.parseDouble(timeIdx[i]));
			};
			arrStatusTime.add(Double.parseDouble(lossTime));
			
			for(int i = 0; i < colorIdx.length; i++){
				arrStatusColor.add(colorIdx[i]);
			};
			arrStatusColor.add("white");
			
			this.first = false;
			
			Date date = new Date();
			SimpleDateFormat sdf3 = new SimpleDateFormat("HHmm");
			String minute2 = sdf3.format(date);
			minute2 = minute2.substring(2,3);
		}else{
			txtFileParsing();
		};
 	};
	
	public void txtFileParsing(){
		this.first = false;
		String txt = "";
		String statusFile = this.today;
		File aFile = new File(statusFile+".txt");
		FileReader fileReader;
		try {
			fileReader = new FileReader(aFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
			return;
		}
		BufferedReader reader = new BufferedReader(fileReader);
		
		String line = null;
		
		try {
			while((line = reader.readLine())!=null){
				txt += line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		};
		
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
		}
		
		//txt 파일 파싱
		String[] index = txt.split("/");
		String timeArray = index[0];
		String colorArray = index[1];
		
		String[] time = timeArray.split(",");
		String[] color = colorArray.split(",");
		
		this.idx = time.length-1;
		
		//배열 초기화
		this.arrStatusTime = new ArrayList<Double>();
		this.arrStatusColor = new ArrayList<String>();
		
		for(int i = 0; i < time.length;i++){
			arrStatusTime.add(Double.parseDouble(time[i]));
		};
		
		for(int i = 0; i < color.length;i++){
			arrStatusColor.add(color[i]);
		};
		
		Date d = new Date();
		SimpleDateFormat sdf2 = new SimpleDateFormat("HHmm");
		String minute = sdf2.format(d);
		minute = minute.substring(2,3);
		this.pMinute = minute;
	};
	
	public void initStatusData(){
		this.arrStatusTime = new ArrayList<Double>();
		this.arrStatusColor = new ArrayList<String>();
		this.idx = 0;
		this.first = true;
		this.pMinute = "";
		this.pStatus = "";
	};
	
	@Override
	@Transactional
	public String addStatus(NFCVo nfcVo) {
		try {
			SqlSession sql=getSqlSession();
			Date d = new Date();

			SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
			String today = sdfNow.format(d); 

			logger.info("run postTest-nfcVo:"+nfcVo);
			logger.info("getTimeStamp:"+nfcVo.getTimeStamp());
			logger.info("getMainProgname:"+nfcVo.getMainProgName());
			logger.info("getNcStatus:"+nfcVo.getNcStatus());
			logger.info("getSpindleLoad:"+nfcVo.getSpindleLoad());
			logger.info("getFeedOverride:"+nfcVo.getFeedOverride());
			logger.info("getAlarmType1:"+nfcVo.getAlarmType1());
			logger.info("getAlarmNumber1:"+nfcVo.getAlarmNumber1());
			logger.info("getAlarmMsg1:"+nfcVo.getAlarmMsg1());
			logger.info("getAlarmType2:"+nfcVo.getAlarmType2());
			logger.info("getAlarmNumber2:"+nfcVo.getAlarmNumber2());
			logger.info("getAlarmMsg2:"+nfcVo.getAlarmMsg2());
			logger.info("getMD5:"+nfcVo.getMD5());
			
			AdapterVo pureVo = nfc2Pure(nfcVo);
			
			if(isDuple(pureVo) == true){
				return "duple";
			}else{
				Integer cnt = (Integer)sql.selectOne(ADAPTER_SPACE+"cntAdapterStatus", pureVo);
				if(cnt.intValue() > 0){
					AdapterVo rtnVo = (AdapterVo) sql.selectOne(ADAPTER_SPACE+"getLastStartTime" , pureVo);
					pureVo.setEndDateTime(rtnVo.getEndDateTime());
					sql.update(ADAPTER_SPACE+"editLastEndTime" , pureVo);
				}
				sql.insert(ADAPTER_SPACE+"addPureData", pureVo);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			e.printStackTrace();
			return "FAIL";
		}
		return "OK";
	}
	
	private AdapterVo nfc2Pure(NFCVo nfcVo){
		AdapterVo rtnPureVo = new AdapterVo();
		
		long unixSeconds = nfcVo.getTimeStamp();
		Date date = new Date(unixSeconds*1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		String cTime = sdf.format(date);
		rtnPureVo.setAdtId(nfcVo.getAdtId());
		rtnPureVo.setStartDateTime(cTime);
		rtnPureVo.setMainPrgmName(nfcVo.getMainProgName());
		
		//rtnPureVo.setStatus(nfcVo.getNcStatus());
		rtnPureVo.setSpdLoad(Float.valueOf(nfcVo.getSpindleLoad()));
		rtnPureVo.setFeedOverride(Integer.valueOf(nfcVo.getFeedOverride()));
		
		//[{"alarmMsg":"","alarmCode":""},{"alarmMsg":"","alarmCode":""}]
		JSONObject alarm1 = new JSONObject();
		alarm1.put("alarmMsg",nfcVo.getAlarmMsg1());
		alarm1.put("alarmCode",nfcVo.getAlarmNumber1());
		JSONObject alarm2 = new JSONObject();
		alarm2.put("alarmMsg",nfcVo.getAlarmMsg2());
		alarm2.put("alarmCode",nfcVo.getAlarmNumber2());
		JSONArray alarm = new JSONArray();
		alarm.add(alarm1.toJSONString());
		alarm.add(alarm2.toJSONString());
		rtnPureVo.setAlarm(alarm.toJSONString());
		
		String status = nfcVo.getNcStatus();
		if(status.equals("STRT") || status.equals("START")){
			rtnPureVo.setStatus(CommonCode.MSG_IN_CYCLE);
		}else if((!status.equals("STRT") || !status.equals("START")) && alarm.equals("")){
			rtnPureVo.setStatus(CommonCode.MSG_WAIT);
		}else if(!alarm.equals("[{\"alarmMsg\":\"\",\"alarmCode\":\"\"},{\"alarmMsg\":\"\",\"alarmCode\":\"\"}]")){
			rtnPureVo.setStatus(CommonCode.MSG_ALARM);
		}
		
		return rtnPureVo;
	}
	
	private boolean isDuple(AdapterVo pureVo){
		
		AdapterVo checkPureVo = mapDupleChecker.get(pureVo.getAdtId());
		if(checkPureVo == null){
			mapDupleChecker.put(pureVo.getAdtId(), pureVo);
			return false;
		}else{
			if(! checkPureVo.getAlarm().equals(pureVo.getAlarm())) return false;
			if(! checkPureVo.getMode().equals(pureVo.getMode())) return false;
			if(! checkPureVo.getStatus().equals(pureVo.getStatus())) return false;
			if(! checkPureVo.getMainPrgmName().equals(pureVo.getMainPrgmName())) return false;
			if(! checkPureVo.getModal().equals(pureVo.getModal())) return false;
			if(! checkPureVo.getSpdLoad().equals(pureVo.getSpdLoad())) return false;
			if(! checkPureVo.getFeedOverride().equals(pureVo.getFeedOverride())) return false;
			if(! checkPureVo.getRapidFeedOverride().equals(pureVo.getRapidFeedOverride())) return false;			
			return true;
		}
	}
	
	@Override
	public List<AdapterVo> getListStatusDate(AdapterVo pureVo) {
		SqlSession sql=getSqlSession();
		
		List<AdapterVo> listStatusVo = sql.selectList(ADAPTER_SPACE+"getListStatusDate" , pureVo);
		
		return listStatusVo;
	}
	
	private void sendGet(String url) throws Exception {
 
		url = "http://175.244.252.137:1234/getParam.cgi?DIStatus_00=?&DIStatus_01=?&DIStatus_02=?&DIStatus_03=?";
 
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
		//add request header
		con.setRequestProperty("User-Agent", "Unomic/cannon");
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
 
	}
}

