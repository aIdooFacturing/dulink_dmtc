package com.unomic.smarti.nfc.service;

import java.util.List;

import com.unomic.cnc.data.MemoryMap;
import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.smarti.nfc.domain.NFCVo;

public interface NFCService {
	public void insertData(MemoryMap Map);
	public void writeStatus(NFCVo nfcVo);
	public String getToday();
	public void setToday(String today);
	public String getTimeStamp();
	public void setTimeStamp(String timeStamp);
	public String getCTime();
	public void setCTime(String cTime);
	public String addStatus(NFCVo nfcVo);
	public List<AdapterVo> getListStatusDate(AdapterVo pureVo);
	
};
