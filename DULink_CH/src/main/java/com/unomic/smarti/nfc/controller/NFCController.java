package com.unomic.smarti.nfc.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.cnc.GlobalData;
import com.unomic.cnc.data.MemoryMap;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.smarti.nfc.domain.Alarm;
import com.unomic.smarti.nfc.domain.NFCVo;
import com.unomic.smarti.nfc.service.NFCService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/nfc")
@Controller
public class NFCController {

	private static final Logger logger = LoggerFactory.getLogger(NFCController.class);
	private final static String namespace= "com.unomic.smarti.nfc,";
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private NFCService nfcService;

	@Autowired
	private GlobalData globalData;

	@RequestMapping(value="conn")
	@ResponseBody
	public String conn(HttpSession session, String ip, String port) throws UnsupportedEncodingException{

		//MemoryMap map = conn.status(new ReqStatusParam(0, 0));
		MemoryMap map = globalData.getStatusMap();

		String status = null;
		String spindleValue = null;
		int feedOverride = 0;
		if(map!=null	){
			spindleValue = map.getSpindleValue();
			feedOverride = map.getFeed_override();
			status = map.getNcStatus();
			try{
				Integer spinIndex = spindleValue.indexOf("rpm");
				spindleValue = spindleValue.substring(0,spinIndex-1);
			}catch(Exception e){
				e.printStackTrace();
			}

			String targetFeed = map.getTargetFeed();
			Integer targetIndex = targetFeed.indexOf(".");
			targetFeed = targetFeed.substring(0, targetIndex);

		}else{
			System.out.println("@cannon fail");
		};

		return String.valueOf(spindleValue) + "/" + 
		feedOverride + "/" + 
		map.getMainProg_name() + "/" + 
		map.getAlarmNumber() + "/" + 
		status + "/" + 
		URLEncoder.encode(map.getAlarmArray(),"utf-8") + "/" + 
		map.getSpindlesValues() + "/" + 
		map.getAlarm();
	};

	@RequestMapping(value="chart2")
	public String chart2(HttpServletRequest request, ModelMap model, HttpSession session){
		String ip = request.getParameter("ip");
		String port = request.getParameter("port");

		session.setAttribute("ip", ip);
		session.setAttribute("port", port);
		return "chart/chart2";
	};
	@RequestMapping(value="chart")
	public String chart(HttpServletRequest request, ModelMap model, HttpSession session){
		String ip = request.getParameter("ip");
		String port = request.getParameter("port");

		session.setAttribute("ip", ip);
		session.setAttribute("port", port);
		return "chart/chart";
	};

	@RequestMapping(value="chart3")
	public String chart3(HttpServletRequest request, ModelMap model, HttpSession session){
		String ip = request.getParameter("ip");
		String port = request.getParameter("port");

		session.setAttribute("ip", ip);
		session.setAttribute("port", port);
		return "chart/chart4";
	};

	@RequestMapping(value="chart4")
	public String chart4(HttpServletRequest request, ModelMap model, HttpSession session){
		String ip = request.getParameter("ip");
		String port = request.getParameter("port");

		session.setAttribute("ip", ip);
		session.setAttribute("port", port);
		return "chart/chart4";
	};

	@RequestMapping(value="getData")
	@ResponseBody
	public String getData() throws IOException{
		String result = "";

		String statusFile = nfcService.getToday();
		if(statusFile.equals("empty")){
			return "fail";
		}
		File aFile = new File(statusFile + CommonCode.FILE_EXTEND);
		FileReader fileReader = new FileReader(aFile);
		BufferedReader reader = new BufferedReader(fileReader);

		logger.info("filePath:"+aFile.getPath());
		String line = null;

		while((line = reader.readLine())!=null){
			result += line;
		}
		reader.close();

		result = result + "/" +nfcService.getToday()+" "+nfcService.getCTime();
		logger.info("result:"+result);
		return result;
	}
	
	@RequestMapping(value="getDbData")
	@ResponseBody
	public String getDbData(String adtId,String date) {
		logger.info("getDbData");
		logger.info("adtId:"+adtId);
		logger.info("date:"+date);
		
		Date today = new Date();
		SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cDateTime = sdfTime.format(today);
		
		String result = "";
		String[] chartFormat;
		AdapterVo inputVo = new AdapterVo();
		inputVo.setAdtId(adtId);
		inputVo.setWorkDate(date);
		List<AdapterVo> listStatus= nfcService.getListStatusDate(inputVo);
		if(listStatus.size() < 1){
			return "noData";
		}else{
			chartFormat = makeChartFormat(listStatus, date);
		}
		
		AdapterVo lastVo = listStatus.get(listStatus.size()-1);
		JSONObject rtnJson = new JSONObject();
		rtnJson.put("statChartTime", chartFormat[0]);
		rtnJson.put("statChartColor", chartFormat[1]);
		rtnJson.put("spindleLoad", lastVo.getSpdLoad());
		rtnJson.put("feedOverride", lastVo.getFeedOverride());
		rtnJson.put("prgmName", lastVo.getMainPrgmName());
		rtnJson.put("chartStatus", lastVo.getChartStatus());
		rtnJson.put("alarm", lastVo.getAlarm());
		rtnJson.put("cDateTime", cDateTime);

		logger.info("result:"+rtnJson.toJSONString());
		return rtnJson.toJSONString();
	}

	/*@RequestMapping("chkDir")
	@ResponseBody
	public String chkDir() throws IOException{
		File aFile = new File("status.txt");
		FileReader fileReader = new FileReader(aFile);
		BufferedReader reader = new BufferedReader(fileReader);

		String line = null;

		String result = aFile.getAbsolutePath();

		while((line = reader.readLine())!=null){
			System.out.println(line);
		}
		reader.close();

		return result;
	};*/

	@RequestMapping(value="receiveData")
	@ResponseBody
	public void receiveData(HttpServletRequest request) throws IOException{
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
		String today = sdf.format(d);

		String data = request.getParameter("data");
		String ip = request.getParameter("ip");

		String[] dataIndex = data.split("/");
		String statusArray = dataIndex[1];
		String currentStatus = statusArray;
		int start = currentStatus.lastIndexOf(",")+1;

		currentStatus = currentStatus.substring(start); 
		logger.info("DATE : " + today + ", IP : " + ip + ", STATUS : " + currentStatus);

		//파일 쓰기
		BufferedWriter statusFile = new BufferedWriter(new FileWriter(nfcService.getToday() + CommonCode.FILE_EXTEND));
		statusFile.write(data);
		statusFile.flush();
		statusFile.close();

		//status data list 삽입
		globalData.resetStatusArray();
		ArrayList<Double> statusTime = globalData.getStatusTime();
		ArrayList<String> statusColors = globalData.getStatusColor();

		String[] index = data.split("/");
		String timeArray = index[0];
		String colorArray = index[1];
		String spindle = index[2];
		String feedOverride = index[3];
		String progName = index[4];
		String alarmNo = index[5];
		String status = index[6];
		String alarmContents = index[7];

		globalData.setSpindle(spindle);
		globalData.setFeed(feedOverride);
		globalData.setProgName(progName);
		globalData.setAlarmNo(alarmNo);
		globalData.setStatus(status);
		globalData.setAlarmContents(alarmContents);

		String[] timeIndex = timeArray.split(",");
		String[] colorIndex = colorArray.split(",");

		for(int i = 0; i < timeIndex.length; i++){
			statusTime.add(Double.parseDouble(timeIndex[i]));
		};

		for(int i = 0; i < colorIndex.length; i++){
			statusColors.add(colorIndex[i]);
		};
	};

	@RequestMapping(value="adapterPost")
	@ResponseBody
	public String postTest(HttpServletRequest request,NFCVo nfcVo){
		logger.info("run postTest-nfcVo:"+nfcVo);
		try {
			Date d = new Date();
			SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
			String today = sdfNow.format(d);
			String ip = request.getRemoteAddr(); 
			logger.info("DATE : " + today + ", IP : " + ip );
			nfcVo.setRemoteIp(ip);
			request.getRequestURL().toString();
			String phrase = request.getRequestURL().toString();
			String delims = "[/]+";
			String[] tokens = phrase.split(delims);
			
//		    if(tokens[2].equals("DULink")){
//		    	nfcVo.setAdtId("ADT-WIN-000001");
//		    }else if(tokens[2].equals("DULink_test")){
//		    	nfcVo.setAdtId("ADT-ARM-000001");
//		    }
		    
		    logger.info("getAdtId:"+nfcVo.getAdtId());
			logger.info("getTimeStamp:"+nfcVo.getTimeStamp());
			logger.info("getMainProgname:"+nfcVo.getMainProgName());
			logger.info("getNcStatus:"+nfcVo.getNcStatus());
			logger.info("getSpindleLoad:"+nfcVo.getSpindleLoad());
			logger.info("getFeedOverride:"+nfcVo.getFeedOverride());
			logger.info("getAlarmType1:"+nfcVo.getAlarmType1());
			logger.info("getAlarmNumber1:"+nfcVo.getAlarmNumber1());
			logger.info("getAlarmMsg1:"+nfcVo.getAlarmMsg1());
			logger.info("getAlarmType2:"+nfcVo.getAlarmType2());
			logger.info("getAlarmNumber2:"+nfcVo.getAlarmNumber2());
			logger.info("getAlarmMsg2:"+nfcVo.getAlarmMsg2());
			logger.info("getMD5:"+nfcVo.getMD5());
			
			List<Alarm> arrAlarm = new ArrayList<Alarm>();
			arrAlarm.add(new Alarm(nfcVo.getAlarmType1(), nfcVo.getAlarmNumber1(), nfcVo.getAlarmMsg1(), nfcVo.getAlarmMtesAction1()));
			arrAlarm.add(new Alarm(nfcVo.getAlarmType2(), nfcVo.getAlarmNumber2(), nfcVo.getAlarmMsg2(), nfcVo.getAlarmMtesAction2()));
			
			nfcVo.setArrAlarm(arrAlarm);
			String tmpMd5 = nfcVo.getAdtId()+nfcVo.getTimeStamp()+nfcVo.getMainProgName()+nfcVo.getNcStatus()
							+nfcVo.getSpindleLoad()+nfcVo.getFeedOverride()
							+nfcVo.getAlarmType1()+nfcVo.getAlarmNumber1()+nfcVo.getAlarmMsg1()
							+nfcVo.getAlarmType2()+nfcVo.getAlarmNumber2()+nfcVo.getAlarmMsg2();
			
			String rstMD5 = makeMD5(tmpMd5);
			//String rstMD5_2 = getMD5EncryptedString(tmpMd5);
			
			if(rstMD5.equals(nfcVo.getMD5())){
				logger.info("MD5 Clear");
				nfcService.writeStatus(nfcVo);
			}else{
				logger.info("MD5 Failed");
				return "FAIL";
			}
			long unixSeconds = nfcVo.getTimeStamp();
			Date date = new Date(unixSeconds*1000L);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
			String cTime = sdf.format(date);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "FAIL";
		}
		return "OK";
	}

	public String makeMD5(String str){
		String MD5 = ""; 
		try{
			MessageDigest md = MessageDigest.getInstance("MD5"); 
			md.update(str.getBytes()); 
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			MD5 = sb.toString();

		}catch(NoSuchAlgorithmException e){
			e.printStackTrace(); 
			MD5 = null; 
		}
		return MD5;
	}
	
	public String getMD5EncryptedString(String encTarget){
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }
	
	@RequestMapping(value="testReq")
	@ResponseBody
	public String reqTest(HttpServletRequest request)
	{
		request.getRequestURL().toString();
		
		String phrase = request.getRequestURL().toString();
		String delims = "[/]+";
		String[] tokens = phrase.split(delims);
		
		
	    return tokens[2];
	}
	
	private String[] makeChartFormat(List<AdapterVo> listStatus, String workDate){
		String statusChecker="In-Cycle";
		String statusColor="green";
		int maxChartSize = 144;
		String CHART_GAP = "0.166";

		int maxIdx=0;
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = sdf.format(d);
		List<String> listChart = new ArrayList<String>();
		
		for(int i = 0;i<maxChartSize;i++){//init list
			listChart.add(i,"Init");
		}
		
		for(int j=0;j<3;j++){//우선순위에 따른 3번의 루프.
			if(j==0){ statusChecker = "In-Cycle"; statusColor = "green";};//First
			if(j==1){ statusChecker = "Wait"; statusColor = "yellow";};//Second
			if(j==2){ statusChecker = "Alarm"; statusColor = "red";};//Third
			for(int i=0,size=listStatus.size();i<size;i++){
				AdapterVo tmpVo = listStatus.get(i);
				if(tmpVo.getChartStatus().equals(statusChecker)){
					//logger.info("ChartStatus is Equals! [i]:["+i+"][j]:["+j+"]");
					//logger.info("status:"+tmpVo.getChartStatus());
					int startIdx=999;
					int endIdx = 0;
					if(i==0){//첫 데이터일 경우.
						startIdx = 0;
					}else{
						startIdx = getIndexChart(workDate,tmpVo.getStartDateTime());
					}
					if(i==size-1){// 마지막 데이터일 경우.
						if(tmpVo.getEndDateTime() == null){//현재 진행형
							endIdx = getIndexChart(workDate,now);
						}else{//과거 데이터
							//listChart.add( maxChartSize-1 , statusChecker);
							endIdx = maxChartSize - 1;
						}
					}else{
						endIdx = getIndexChart(workDate,tmpVo.getEndDateTime());
					}
					if(endIdx > maxIdx) { maxIdx = endIdx; }
					//logger.info("startIdx:"+startIdx+"/endIdx:"+endIdx);
					for(int k = startIdx;k<endIdx;k++){//첫 Idx부터 마지막 Idx까지 채우기.
						//logger.info("run K loop");
						listChart.add(k,statusColor);
					}
				}
				tmpVo.getStartDateTime();
			}
		}
		
		String[] chartFormat={"",""};
		chartFormat[0]="0.0";
		for(int i = 0 ; i < maxIdx ; i++){
			chartFormat[0] += ","+CHART_GAP;
		}
		chartFormat[1] = listChart.get(0);
		for(int i = 0 ; i < maxIdx ; i++){
			chartFormat[1] += ","+listChart.get(i);
		}
		return chartFormat;
	}
	
	private int getIndexChart(String workDate, String targetDateTime){
		
		String timeInit = "08:00:00.000";
		String initDateTime = workDate+" "+timeInit;		

		Calendar tempcal=Calendar.getInstance();
		SimpleDateFormat sf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startday = sf.parse(initDateTime, new ParsePosition(0));
		Date endday = sf.parse(targetDateTime, new ParsePosition(0));

		long startTime = startday.getTime();
		long endTime = endday.getTime();
		long mills = endTime - startTime;
		//분으로 변환
		long min = mills / 60000;
		
		int rtnIdx = new BigDecimal(min/10).intValueExact();
		return rtnIdx;
	}
	
};

