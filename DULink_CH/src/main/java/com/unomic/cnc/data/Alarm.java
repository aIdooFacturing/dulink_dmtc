package com.unomic.cnc.data;

public class Alarm 
{
	int alarm_type;
	byte alarm_number[] = new byte[8];		// 8 byte
	byte alarm_message[] = new byte[256];	// 256 byte 
	byte mtes_action;

    public Alarm(int alarm_type, byte alarm_number[], byte alarm_message[], byte mtes_action)
	{
		this.alarm_type = alarm_type;
		this.alarm_number = alarm_number;
		this.alarm_message = alarm_message;
		this.mtes_action = mtes_action;
	}

	public int getAlarm_type() 
	{
		return alarm_type;
	}

	public void setAlarm_type(int alarm_type) 
	{
		this.alarm_type = alarm_type;
	}

	public byte[] getAlarm_number() 
	{
		return alarm_number;
	}

	public void setAlarm_number(byte[] alarm_number) 
	{
		this.alarm_number = alarm_number;
	}

	public byte[] getAlarm_message() 
	{
		return alarm_message;
	}

	public void setAlarm_message(byte[] alarm_message) 
	{
		this.alarm_message = alarm_message;
	}
	
    public byte getMtes_action()
    {
        return mtes_action;
    }

    public void setMtes_action(byte mtes_action)
    {
        this.mtes_action = mtes_action;
    }	
}
