//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * 
 *         Abstract cutter life
 *       
 * 
 * <p>LifeType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="LifeType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;urn:mtconnect.org:MTConnectAssets:1.3>ToolLifeValueType">
 *       &lt;attribute name="type" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}ToolLifeType" />
 *       &lt;attribute name="countDirection" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}ToolLifeDirectionType" />
 *       &lt;attribute name="warning" type="{urn:mtconnect.org:MTConnectAssets:1.3}ToolLifeValueType" />
 *       &lt;attribute name="limit" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}ToolLifeValueType" />
 *       &lt;attribute name="initial" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}ToolLifeValueType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LifeType", propOrder = {
    "value"
})
public class LifeType {

    @XmlValue
    protected float value;
    @XmlAttribute(name = "type", required = true)
    protected ToolLifeType type;
    @XmlAttribute(name = "countDirection", required = true)
    protected ToolLifeDirectionType countDirection;
    @XmlAttribute(name = "warning")
    protected Float warning;
    @XmlAttribute(name = "limit", required = true)
    protected float limit;
    @XmlAttribute(name = "initial", required = true)
    protected float initial;

    /**
     * 
     *         The life of the tool in time, wear, or parts
     *       
     * 
     */
    public float getValue() {
        return value;
    }

    /**
     * value 속성의 값을 설정합니다.
     * 
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * type 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ToolLifeType }
     *     
     */
    public ToolLifeType getType() {
        return type;
    }

    /**
     * type 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ToolLifeType }
     *     
     */
    public void setType(ToolLifeType value) {
        this.type = value;
    }

    /**
     * countDirection 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ToolLifeDirectionType }
     *     
     */
    public ToolLifeDirectionType getCountDirection() {
        return countDirection;
    }

    /**
     * countDirection 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ToolLifeDirectionType }
     *     
     */
    public void setCountDirection(ToolLifeDirectionType value) {
        this.countDirection = value;
    }

    /**
     * warning 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getWarning() {
        return warning;
    }

    /**
     * warning 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setWarning(Float value) {
        this.warning = value;
    }

    /**
     * limit 속성의 값을 가져옵니다.
     * 
     */
    public float getLimit() {
        return limit;
    }

    /**
     * limit 속성의 값을 설정합니다.
     * 
     */
    public void setLimit(float value) {
        this.limit = value;
    }

    /**
     * initial 속성의 값을 가져옵니다.
     * 
     */
    public float getInitial() {
        return initial;
    }

    /**
     * initial 속성의 값을 설정합니다.
     * 
     */
    public void setInitial(float value) {
        this.initial = value;
    }

}
