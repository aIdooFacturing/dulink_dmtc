//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         An abstract time series with the restriction value
 *       
 * 
 * <p>TimeSeriesType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="TimeSeriesType">
 *   &lt;simpleContent>
 *     &lt;restriction base="&lt;urn:mtconnect.org:MTConnectStreams:1.3>AbsTimeSeriesType">
 *     &lt;/restriction>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSeriesType")
@XmlSeeAlso({
    AccelerationTimeSeriesType.class,
    PathFeedrateTimeSeriesType.class,
    AngularVelocityTimeSeriesType.class,
    PathPositionTimeSeriesType.class,
    FlowTimeSeriesType.class,
    FrequencyTimeSeriesType.class,
    ResistanceTimeSeriesType.class,
    VoltsTimeSeriesType.class,
    TorqueTimeSeriesType.class,
    SpindleSpeedTimeSeriesType.class,
    TiltTimeSeriesType.class,
    AxisFeedrateTimeSeriesType.class,
    WattTimeSeriesType.class,
    AccumulatedTimeTimeSeriesType.class,
    PositionTimeSeriesType.class,
    ElectricalEnergyTimeSeriesType.class,
    AmperageTimeSeriesType.class,
    GlobalPositionTimeSeriesType.class,
    LinearForceTimeSeriesType.class,
    VoltageTimeSeriesType.class,
    PressureTimeSeriesType.class,
    StrainTimeSeriesType.class,
    SoundPressureTimeSeriesType.class,
    VelocityTimeSeriesType.class,
    AngleTimeSeriesType.class,
    TemperatureTimeSeriesType.class,
    FillLevelTimeSeriesType.class,
    RotaryVelocityTimeSeriesType.class,
    AbsTimeSeriesTimeSeriesType.class,
    WattageTimeSeriesType.class,
    LengthTimeSeriesType.class,
    AngularAccelerationTimeSeriesType.class,
    LoadTimeSeriesType.class,
    ConcentrationTimeSeriesType.class,
    ViscosityTimeSeriesType.class,
    ConductivityTimeSeriesType.class,
    PowerFactorTimeSeriesType.class,
    DisplacementTimeSeriesType.class
})
public abstract class TimeSeriesType
    extends AbsTimeSeriesType
{


}
