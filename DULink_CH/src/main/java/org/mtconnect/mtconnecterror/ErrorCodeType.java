//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:29 AM KST 
//


package org.mtconnect.mtconnecterror;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ErrorCodeType에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * <p>
 * <pre>
 * &lt;simpleType name="ErrorCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNAUTHORIZED"/>
 *     &lt;enumeration value="NO_DEVICE"/>
 *     &lt;enumeration value="OUT_OF_RANGE"/>
 *     &lt;enumeration value="TOO_MANY"/>
 *     &lt;enumeration value="INVALID_URI"/>
 *     &lt;enumeration value="INVALID_REQUEST"/>
 *     &lt;enumeration value="INTERNAL_ERROR"/>
 *     &lt;enumeration value="INVALID_PATH"/>
 *     &lt;enumeration value="UNSUPPORTED"/>
 *     &lt;enumeration value="ASSET_NOT_FOUND"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ErrorCodeType")
@XmlEnum
public enum ErrorCodeType {

    UNAUTHORIZED,
    NO_DEVICE,
    OUT_OF_RANGE,
    TOO_MANY,
    INVALID_URI,
    INVALID_REQUEST,
    INTERNAL_ERROR,
    INVALID_PATH,
    UNSUPPORTED,
    ASSET_NOT_FOUND;

    public String value() {
        return name();
    }

    public static ErrorCodeType fromValue(String v) {
        return valueOf(v);
    }

}
