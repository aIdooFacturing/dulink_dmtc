//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:02 AM KST 
//


package org.mtconnect.mtconnectdevices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         An abstract component type. This is a placeholder for all components
 *       
 * 
 * <p>ComponentType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="ComponentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{urn:mtconnect.org:MTConnectDevices:1.3}ComponentDescriptionType" minOccurs="0"/>
 *         &lt;element name="Configuration" type="{urn:mtconnect.org:MTConnectDevices:1.3}ComponentConfigurationType" minOccurs="0"/>
 *         &lt;element name="DataItems" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemsType" minOccurs="0"/>
 *         &lt;element name="Components" type="{urn:mtconnect.org:MTConnectDevices:1.3}ComponentsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}IDType" />
 *       &lt;attribute name="nativeName" type="{urn:mtconnect.org:MTConnectDevices:1.3}NameType" />
 *       &lt;attribute name="sampleInterval" type="{urn:mtconnect.org:MTConnectDevices:1.3}SampleIntervalType" />
 *       &lt;attribute name="sampleRate" type="{urn:mtconnect.org:MTConnectDevices:1.3}SampleRateType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentType", propOrder = {
    "description",
    "configuration",
    "dataItems",
    "components"
})
@XmlSeeAlso({
    DeviceType.class,
    CommonComponentType.class
})
public abstract class ComponentType {

    @XmlElement(name = "Description")
    protected ComponentDescriptionType description;
    @XmlElement(name = "Configuration")
    protected ComponentConfigurationType configuration;
    @XmlElement(name = "DataItems")
    protected DataItemsType dataItems;
    @XmlElement(name = "Components")
    protected ComponentsType components;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;
    @XmlAttribute(name = "nativeName")
    protected String nativeName;
    @XmlAttribute(name = "sampleInterval")
    protected Float sampleInterval;
    @XmlAttribute(name = "sampleRate")
    protected Float sampleRate;

    /**
     * description 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ComponentDescriptionType }
     *     
     */
    public ComponentDescriptionType getDescription() {
        return description;
    }

    /**
     * description 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentDescriptionType }
     *     
     */
    public void setDescription(ComponentDescriptionType value) {
        this.description = value;
    }

    /**
     * configuration 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ComponentConfigurationType }
     *     
     */
    public ComponentConfigurationType getConfiguration() {
        return configuration;
    }

    /**
     * configuration 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentConfigurationType }
     *     
     */
    public void setConfiguration(ComponentConfigurationType value) {
        this.configuration = value;
    }

    /**
     * dataItems 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link DataItemsType }
     *     
     */
    public DataItemsType getDataItems() {
        return dataItems;
    }

    /**
     * dataItems 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link DataItemsType }
     *     
     */
    public void setDataItems(DataItemsType value) {
        this.dataItems = value;
    }

    /**
     * components 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ComponentsType }
     *     
     */
    public ComponentsType getComponents() {
        return components;
    }

    /**
     * components 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentsType }
     *     
     */
    public void setComponents(ComponentsType value) {
        this.components = value;
    }

    /**
     * id 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * id 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * nativeName 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeName() {
        return nativeName;
    }

    /**
     * nativeName 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeName(String value) {
        this.nativeName = value;
    }

    /**
     * sampleInterval 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getSampleInterval() {
        return sampleInterval;
    }

    /**
     * sampleInterval 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setSampleInterval(Float value) {
        this.sampleInterval = value;
    }

    /**
     * sampleRate 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getSampleRate() {
        return sampleRate;
    }

    /**
     * sampleRate 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setSampleRate(Float value) {
        this.sampleRate = value;
    }

}
