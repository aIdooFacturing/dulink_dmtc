//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:02 AM KST 
//


package org.mtconnect.mtconnectdevices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         A abstract measurement
 *       
 * 
 * <p>DataItemType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="DataItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{urn:mtconnect.org:MTConnectDevices:1.3}SourceType" minOccurs="0"/>
 *         &lt;element name="Constraints" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemConstraintsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{urn:mtconnect.org:MTConnectDevices:1.3}NameType" />
 *       &lt;attribute name="id" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}IDType" />
 *       &lt;attribute name="type" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemEnumType" />
 *       &lt;attribute name="subType" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemSubEnumType" />
 *       &lt;attribute name="statistic" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemStatisticsType" />
 *       &lt;attribute name="units" type="{urn:mtconnect.org:MTConnectDevices:1.3}UnitsType" />
 *       &lt;attribute name="nativeUnits" type="{urn:mtconnect.org:MTConnectDevices:1.3}NativeUnitsType" />
 *       &lt;attribute name="nativeScale" type="{urn:mtconnect.org:MTConnectDevices:1.3}NativeScaleType" />
 *       &lt;attribute name="category" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}CategoryType" />
 *       &lt;attribute name="coordinateSystem" type="{urn:mtconnect.org:MTConnectDevices:1.3}CoordinateSystemType" />
 *       &lt;attribute name="sampleRate" type="{urn:mtconnect.org:MTConnectDevices:1.3}SampleRateType" />
 *       &lt;attribute name="representation" type="{urn:mtconnect.org:MTConnectDevices:1.3}RepresentationType" default="VALUE" />
 *       &lt;attribute name="significantDigits" type="{urn:mtconnect.org:MTConnectDevices:1.3}SignificantDigitsValueType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataItemType", propOrder = {
    "source",
    "constraints"
})
public class DataItemType {

    @XmlElement(name = "Source")
    protected SourceType source;
    @XmlElement(name = "Constraints")
    protected DataItemConstraintsType constraints;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;
    @XmlAttribute(name = "type", required = true)
    protected String type;
    @XmlAttribute(name = "subType")
    protected String subType;
    @XmlAttribute(name = "statistic")
    protected String statistic;
    @XmlAttribute(name = "units")
    protected String units;
    @XmlAttribute(name = "nativeUnits")
    protected String nativeUnits;
    @XmlAttribute(name = "nativeScale")
    protected Float nativeScale;
    @XmlAttribute(name = "category", required = true)
    protected CategoryType category;
    @XmlAttribute(name = "coordinateSystem")
    protected CoordinateSystemType coordinateSystem;
    @XmlAttribute(name = "sampleRate")
    protected Float sampleRate;
    @XmlAttribute(name = "representation")
    protected RepresentationType representation;
    @XmlAttribute(name = "significantDigits")
    protected BigInteger significantDigits;

    /**
     * source 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link SourceType }
     *     
     */
    public SourceType getSource() {
        return source;
    }

    /**
     * source 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceType }
     *     
     */
    public void setSource(SourceType value) {
        this.source = value;
    }

    /**
     * constraints 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link DataItemConstraintsType }
     *     
     */
    public DataItemConstraintsType getConstraints() {
        return constraints;
    }

    /**
     * constraints 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link DataItemConstraintsType }
     *     
     */
    public void setConstraints(DataItemConstraintsType value) {
        this.constraints = value;
    }

    /**
     * name 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * name 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * id 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * id 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * type 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * type 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * subType 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubType() {
        return subType;
    }

    /**
     * subType 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubType(String value) {
        this.subType = value;
    }

    /**
     * statistic 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatistic() {
        return statistic;
    }

    /**
     * statistic 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatistic(String value) {
        this.statistic = value;
    }

    /**
     * units 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnits() {
        return units;
    }

    /**
     * units 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnits(String value) {
        this.units = value;
    }

    /**
     * nativeUnits 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeUnits() {
        return nativeUnits;
    }

    /**
     * nativeUnits 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeUnits(String value) {
        this.nativeUnits = value;
    }

    /**
     * nativeScale 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getNativeScale() {
        return nativeScale;
    }

    /**
     * nativeScale 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setNativeScale(Float value) {
        this.nativeScale = value;
    }

    /**
     * category 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link CategoryType }
     *     
     */
    public CategoryType getCategory() {
        return category;
    }

    /**
     * category 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryType }
     *     
     */
    public void setCategory(CategoryType value) {
        this.category = value;
    }

    /**
     * coordinateSystem 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link CoordinateSystemType }
     *     
     */
    public CoordinateSystemType getCoordinateSystem() {
        return coordinateSystem;
    }

    /**
     * coordinateSystem 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordinateSystemType }
     *     
     */
    public void setCoordinateSystem(CoordinateSystemType value) {
        this.coordinateSystem = value;
    }

    /**
     * sampleRate 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getSampleRate() {
        return sampleRate;
    }

    /**
     * sampleRate 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setSampleRate(Float value) {
        this.sampleRate = value;
    }

    /**
     * representation 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link RepresentationType }
     *     
     */
    public RepresentationType getRepresentation() {
        if (representation == null) {
            return RepresentationType.VALUE;
        } else {
            return representation;
        }
    }

    /**
     * representation 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link RepresentationType }
     *     
     */
    public void setRepresentation(RepresentationType value) {
        this.representation = value;
    }

    /**
     * significantDigits 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSignificantDigits() {
        return significantDigits;
    }

    /**
     * significantDigits 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSignificantDigits(BigInteger value) {
        this.significantDigits = value;
    }

}
