$(function(){
	setElement4();
	
	setInterval(time4, 1000);
	getList();
	setInterval(getList, 1000 * 10);
});

var cnt = 15;
var cList = 0;
var totalList = 0;

function getList(){
	var url = ctxPath + "/mes/mesTest.do";
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "text",
		success : function(data){
			var json = $.parseJSON(data);
			
			var tr = "";
			var tHead = "<tr style='color: black; background-color: #fff8dc; font-weight: bold; font-size: " + getElSize(30) + "px' class='tr4'>"+
							"<td   align='center' class='td4'>"+
								"오더번호"+
							"</td>"+
							"<td   align='center'>"+
								"품번"+
							"</td>"+
							"<td   align='center'>"+
								"품명"+
							"</td>"+
							"<td   align='center'>"+
								"소요일"+
							"</td>"+
							"<td   align='center'>"+
								"소요기종호기"+
							"</td>"+
							"<td   align='center'>"+
								"생산월"+
							"</td>"+
						"</tr>";
			
			$(json).each(function(i, data){
				totalList = i;
				if(i>=(cnt-15) && i<cnt){
					cList++;
					tr += "<tr style='font-size: " + getElSize(25) + "px; font-weight: bold;'>" + 
					"<td style='padding: " + getElSize(10) + "px;'>" + data.aufnr + "</td>" + 
					"<td style='padding: " + getElSize(10) + "px;'>" + data.matnr + "</td>" + 
					"<td style='padding: " + getElSize(10) + "px;'>" + data.maktx + "</td>" + 
					"<td style='padding: " + getElSize(10) + "px;'>" + data.bdter + "</td>" + 
					"<td style='padding: " + getElSize(10) + "px;'>" + data.equnr + "</td>" + 
					"<td style='padding: " + getElSize(10) + "px;'>" + data.emonu + "</td>" + 
					"</tr>";
				}
			});
			
			cnt += 15
			$("#listCnt").html((cList) + "/" + (totalList+1));
			$("#mainTable4").html(tHead + tr);
			
			if(cnt>=totalList){
				cnt = 15;
				cList = 0;
			};
		}
	});
};

function setElement4(){
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	$("#title4").css({
		"font-size" : getElSize(60),
		"margin-top" : (getElSize(25))
	});
	
	$("#title_left4").css({
		"width" : getElSize(150)
	});
	
	$("#title_left4").css({
		"left" : getElSize(25),
		"margin-top" : (getElSize(25))
	});

	$("#title_right4").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(20),
		"margin-top" : getElSize(40),
		"position" : "absolute"
	});

	$("#title_right4").css({
		"left" : contentWidth/2 - $("#title_right").width() - getElSize(25)    
	});

	
	$("#hr4").css({
		"margin-top" : getElSize(50)
	});
	
	$("#time4").css({
		"right" : getElSize(25),
		"top" : (getElSize(85)),
		"font-size" : getElSize(15)
	});
	
	$("#date4").css({
		"right" : getElSize(150),
		"top" : (getElSize(85)),
		"font-size" : getElSize(15)
	});
	
	$("#listCnt").css({
		"font-size" : getElSize(25),
		"right" : getElSize(100)
	});
	
	$("#mainTable4").css({
		"margin-top": getElSize(80)
	});
	
	$(".td4").css({
		"font-size" : getElSize(30),
		"padding" : getElSize(10)
	});
};

function time4(){
	var date = new Date();
	var month = date.getMonth()+1;
	var date_ = addZero(String(date.getDate()));
	var day = date.getDay();
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	if(day==1){
		day = "Mon";
	}else if(day==2){
		day = "Tue";
	}else if(day==3){
		day = "Wed";
	}else if(day==4){
		day = "Thu";
	}else if(day==5){
		day = "Fri";
	}else if(day==6){
		day = "Sat";
	}else if(day==0){
		day = "Sun";
	};
	
	$("#date4").html(month + " / " + date_ + " (" + day + ")");
	$("#time4").html(hour + " : " + minute + " : " + second);
};