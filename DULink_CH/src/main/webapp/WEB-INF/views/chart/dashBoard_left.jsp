<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>DIMF Camera</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
body{
	padding: 0px;
	margin: 0px;
};

</style> 
<script type="text/javascript">
	$(function(){
		init();
	});
	
	function init(){
		var url = "${ctxPath}/chart/initVideoPolling.do";
		
		$.ajax({
			url : url,
			type : "post",
			success :function(){
				polling();
			}
		});
	};
	
	var flag = true;
	var preMachine = 0;
	function polling(){
		var url = "${ctxPath}/chart/polling1.do";
		
		$.ajax({
			url : url,
			dataType : "text",
			type : "post",
			success : function(data){
				if(data==-1){
					stop();	
				};
				
				if(!flag && data==0){
					location.reload();
				};
				if(preMachine!=data){
					play(data);
					preMachine=data;
					flag = false;
				};
				setTimeout(polling, 500);			
			}
		});
		
	};
	
	function stop(){
		var video = document.getElementById("video");
		var src = "";
		$("#video").attr("src", src);
	};
	
	function play(data){
		var video = document.getElementById("video");
		var src = "";
		if(data==44){
			src = "http://10.33.48.202/mjpg/video.mjpg"; 
		}else if(data==45){
			src = "http://10.33.48.202/mjpg/video.mjpg";
		}else if(data==46){
			src = "http://10.33.48.202/mjpg/video.mjpg";
		}

		$("#video").attr("src", src);
	};
	
</script>
</head>
<body>
	<!-- <video src="" width="100%" id="video"></video> -->
	<img src="http://10.33.48.202/mjpg/video.mjpg" width="100%" id="video"></img>
</body>
</html>